﻿# Host: 192.168.11.182  (Version 5.5.5-10.2.7-MariaDB-10.2.7+maria~xenial-log)
# Date: 2019-05-27 09:16:26
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "bpjsdiagnosa"
#

CREATE TABLE `bpjsdiagnosa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bpjsolah_id` int(11) NOT NULL DEFAULT 0,
  `kodeicd` varchar(255) NOT NULL DEFAULT '-',
  `namaicd` varchar(255) NOT NULL DEFAULT '-',
  `status` char(1) NOT NULL DEFAULT 'L',
  `adddate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

#
# Data for table "bpjsdiagnosa"
#

INSERT INTO `bpjsdiagnosa` VALUES (1,1,'aa','rt','L','2018-08-27 00:00:00','2018-08-27 00:00:00',0),(2,1,'bb','tr','B','2018-08-27 13:14:52','2018-08-27 13:14:52',0),(3,5,'01','test diag','L','2018-08-29 11:48:25','2018-08-29 11:48:25',0),(6,8,'12','aaaaa','L','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(7,8,'13','bbbbb','B','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(8,8,'14','cccccc','L','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(9,8,'15','dddd','B','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(10,8,'16','eeee','L','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(16,14,'12','sssss','L','2018-08-31 10:10:46','2018-08-31 10:10:46',0);

#
# Structure for table "bpjsolahdata"
#

CREATE TABLE `bpjsolahdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pasien_id` int(11) NOT NULL DEFAULT 0,
  `noregister` varchar(255) NOT NULL DEFAULT '-',
  `dokter_id` int(11) NOT NULL DEFAULT 0,
  `tanggalperiksa` date NOT NULL DEFAULT '0000-00-00',
  `adddate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

#
# Data for table "bpjsolahdata"
#

INSERT INTO `bpjsolahdata` VALUES (1,1,'RJ1808250005',1,'2018-08-27','2018-08-27 00:00:00','2018-08-31 07:58:04',0),(4,1,'RJ1808250000',1,'2018-08-27','2018-08-27 13:13:36','2018-08-27 13:13:36',0),(5,1,'RJ1808040003',1,'2018-08-04','2018-08-29 11:48:25','2018-08-29 11:48:25',0),(8,1,'RJ1808280001',15,'2018-08-04','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(14,1,'RJ1808250002',2,'2018-08-04','2018-08-31 10:10:46','2018-08-31 10:10:46',0);

#
# Structure for table "bpjstindakan"
#

CREATE TABLE `bpjstindakan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bpjsolah_id` int(11) NOT NULL DEFAULT 0,
  `kodetindakan` varchar(255) NOT NULL DEFAULT '-',
  `namatindakan` varchar(255) NOT NULL DEFAULT '-',
  `adddate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

#
# Data for table "bpjstindakan"
#

INSERT INTO `bpjstindakan` VALUES (1,1,'51','qw','2018-08-27 00:00:00','2018-08-27 00:00:00',0),(2,1,'22','aa','2018-08-27 13:14:55','2018-08-27 13:14:55',0),(3,5,'02','test tind','2018-08-29 11:48:25','2018-08-29 11:48:25',0),(4,8,'17','ffff','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(5,8,'18','gggg','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(6,8,'19','hhhh','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(7,8,'20','iiii','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(8,8,'21','jjjj','2018-08-31 09:36:15','2018-08-31 09:36:15',0),(10,14,'45','fghfgh','2018-08-31 10:10:46','2018-08-31 10:10:46',0);

#
# Structure for table "olahmr"
#

CREATE TABLE `olahmr` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `idkunjungan` varchar(40) DEFAULT NULL,
  `noregister` varchar(40) DEFAULT NULL,
  `nomr` varchar(40) DEFAULT NULL,
  `namapasien` varchar(255) DEFAULT NULL,
  `tanggalperiksa` date DEFAULT NULL,
  `iddokter` varchar(20) DEFAULT NULL,
  `tglsave` timestamp NULL DEFAULT current_timestamp(),
  `tglupdate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

#
# Data for table "olahmr"
#

INSERT INTO `olahmr` VALUES (3,'4','RJ1808250005','1146','Nita','2018-09-10','2',NULL,'2018-09-10 10:23:42',1),(4,'0','RJ0000000000','0000','TEST','2018-09-08','0','2018-09-10 09:41:43','2018-09-15 13:14:13',1),(5,'591','RJ1811010003','1129','bintang','2018-11-01','6','2018-11-02 10:18:28',NULL,1),(6,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:30:09',NULL,0),(7,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:30:22',NULL,0),(8,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:30:26',NULL,0),(9,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:30:29',NULL,1),(10,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:32:40',NULL,1),(11,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:38:04',NULL,0),(12,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:38:10',NULL,0),(13,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 13:38:12',NULL,1),(14,'460','RJ1811050008','1317','Nathania','2018-10-19','7','2018-11-07 14:07:23',NULL,0),(15,'622','RJ1811050008','1317','Nathania','2018-11-05','1','2018-11-07 14:20:53',NULL,1),(16,'622','RJ1811050008','1317','Nathania','2018-11-05','1','2018-11-07 14:28:46',NULL,0),(17,'460','RJ1810190014','1317','Nathania','2018-10-19','7','2018-11-07 14:30:23',NULL,0),(18,'481','RJ1810220006','1317','Nathania','2018-10-22','1','2018-11-07 14:30:36',NULL,0),(19,'629','RJ1811060005','1226','Mawar melati','2018-11-06','6','2018-11-07 14:32:09',NULL,0),(20,'636','RJ1811070008','1166','hendrawan','2018-11-07','5','2018-11-09 15:49:54',NULL,0),(21,'636','RJ1811070008','1166','hendrawan','2018-11-07','5','2018-11-09 15:49:57',NULL,0),(22,'636','RJ1811070008','1166','hendrawan','2018-11-07','5','2018-11-09 15:50:33',NULL,0),(23,'636','RJ1811070008','1166','hendrawan','2018-11-07','5','2018-11-09 15:53:17',NULL,0),(24,'636','RJ1811070008','1166','hendrawan','2018-11-07','5','2018-11-09 15:53:40',NULL,1),(25,'636','RJ1811070008','1166','hendrawan','2018-11-07','5','2018-11-09 15:55:23',NULL,1),(26,'1037','RJ1901250005','1228','nit not','2019-01-25','14','2019-01-30 09:33:20',NULL,1),(27,'128','RJ1901290005','1430','Kanisius Totok','2019-01-29','3','2019-01-30 09:38:31',NULL,1),(28,'110','RJ1901250013','1430','Kanisius Totok','2019-01-25','2','2019-01-30 10:31:16',NULL,1),(29,'108','RJ1901250002','1402','Yuuna','2019-01-25','10','2019-01-30 10:51:16',NULL,1);

#
# Structure for table "diagnosa"
#

CREATE TABLE `diagnosa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `idolahmr` int(11) DEFAULT NULL,
  `kodeicd` varchar(255) DEFAULT NULL,
  `namaicd` varchar(255) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `diagnosaolahmr` (`idolahmr`),
  CONSTRAINT `diagnosaolahmr` FOREIGN KEY (`idolahmr`) REFERENCES `olahmr` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

#
# Data for table "diagnosa"
#

INSERT INTO `diagnosa` VALUES (1,3,'28.11','BIOPSI TONSIL','L',0),(2,3,'M21.4','FLAT FOOT','B',0),(3,3,'Q43.9','MALDIGEST','L',0),(4,4,'TEST','TEST','L',0),(5,5,'28.11','BIOPSI TONSIL','L',0),(6,5,'35.50','CLOSSURE VSD (VENTRIKEL SEPTAL DEFECT)','L',0),(7,5,'03.98','REMOVAL VP SHUNT','L',0),(8,5,'Z73.0','BURN-OUT','L',0),(9,6,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(10,6,'N81.8','PROLAPSE PELVIC','L',0),(11,7,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(12,7,'N81.8','PROLAPSE PELVIC','L',0),(13,7,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(14,8,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(15,8,'N81.8','PROLAPSE PELVIC','L',0),(16,8,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(17,9,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(18,9,'N81.8','PROLAPSE PELVIC','L',0),(19,9,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(20,10,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(21,10,'N81.8','PROLAPSE PELVIC','L',0),(22,11,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(23,12,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(24,13,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(25,14,'28.11','BIOPSI TONSIL','B',0),(26,14,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(27,15,'28.11','BIOPSI TONSIL','B',0),(28,16,'28.11','BIOPSI TONSIL','B',0),(29,16,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','B',0),(30,19,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(31,20,'28.11','BIOPSI TONSIL','B',0),(32,21,'28.11','BIOPSI TONSIL','B',0),(33,22,'28.11','BIOPSI TONSIL','B',0),(34,22,'E88.8','MALODOURS','B',0),(35,23,'28.11','BIOPSI TONSIL','B',0),(36,23,'E88.8','MALODOURS','L',0),(37,24,'28.11','BIOPSI TONSIL','B',0),(38,24,'E88.8','MALODOURS','L',0),(39,25,'28.11','BIOPSI TONSIL','B',0),(40,26,'28.11','BIOPSI TONSIL','L',0),(41,26,'I50.9','BIVENTRICULAR FAILURE','L',0),(42,27,'H26.9','CATARACT IMMATURE','L',0),(43,27,'D48.7','TM LACRIMAL DUCT (UNCERTAIN)','L',0),(44,28,'F43.0','SHOCK PSYCHIS','L',0),(45,28,'F43.0','SHOCK PSYCHIS','L',0),(46,29,'H53.2','DIPLOPIA','L',0),(47,29,'H53.2','DIPLOPIA','L',0);

#
# Structure for table "statuskunjungan"
#

CREATE TABLE `statuskunjungan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pasien_id` int(11) NOT NULL DEFAULT 0,
  `poli` varchar(255) NOT NULL DEFAULT '',
  `jumlahkunjungan` int(11) NOT NULL DEFAULT 0,
  `adddate` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "statuskunjungan"
#

INSERT INTO `statuskunjungan` VALUES (1,2,'Jantung',0,'2018-10-16 00:00:00');

#
# Structure for table "tanggallibur"
#

CREATE TABLE `tanggallibur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hari` varchar(255) NOT NULL DEFAULT '',
  `bulan` varchar(2) NOT NULL DEFAULT '01',
  `tahun` varchar(4) NOT NULL DEFAULT '',
  `tanggal` varchar(50) NOT NULL DEFAULT '0000-00-00',
  `adddate` varchar(50) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "tanggallibur"
#

INSERT INTO `tanggallibur` VALUES (1,'Senin','09','2018','2018-09-03','2018-09-01 00:00:00',0),(3,'Kamis','09','2018','2018-09-06','2018-09-06 13:29:14',0);

#
# Structure for table "tindakan"
#

CREATE TABLE `tindakan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `idolahmr` int(11) DEFAULT NULL,
  `kodetindakan` varchar(255) DEFAULT NULL,
  `namatindakan` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `tindakanolahmr` (`idolahmr`),
  CONSTRAINT `tindakanolahmr` FOREIGN KEY (`idolahmr`) REFERENCES `olahmr` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

#
# Data for table "tindakan"
#

INSERT INTO `tindakan` VALUES (1,3,'28.0','DRAINAGE PERITONSILLAR',0),(2,3,'E88.8','MALODOURS',0),(3,5,'79.60','DEBRIDEMENT FR SKULL TERBUKA',0),(4,5,'N81.8','PROLAPSE PELVIC',0),(5,22,'E88.8','MALODOURS',0),(6,23,'E54','DEFICIENCY VIT C',0),(7,26,'79.60','DEBRIDEMENT FR SKULL TERBUKA',0),(8,27,'88.94','MRI LUTUT',0),(9,28,'H16.0','MOOREN\'S ULCER',0),(10,29,'H16.0','MOOREN\'S ULCER',0);
