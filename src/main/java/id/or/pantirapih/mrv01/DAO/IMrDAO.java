package id.or.pantirapih.mrv01.DAO;


import java.net.MalformedURLException;
import java.sql.Date;
import java.util.List;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;

import id.or.pantirapih.mrv01.Controller.BpjsOlahDataController;
import id.or.pantirapih.mrv01.Model.BpjsDiagnosa;
import id.or.pantirapih.mrv01.Model.BpjsOlahData;
import id.or.pantirapih.mrv01.Model.BpjsTindakan;
import id.or.pantirapih.mrv01.Model.BpjsWrapper;
import id.or.pantirapih.mrv01.Model.Diagnosa;
import id.or.pantirapih.mrv01.Model.Olahmr;
import id.or.pantirapih.mrv01.Model.StatusKunjungan;
import id.or.pantirapih.mrv01.Model.TanggalLibur;
import id.or.pantirapih.mrv01.Model.Tindakan;
import id.or.pantirapih.mrv01.Service.IMrService;
import id.or.pantirapih.mrv01.Service.MrService;


public interface IMrDAO {

	void tambahDataDiagnosa(Diagnosa id) throws MalformedURLException, ParseException;

	void tambahDataTindakan(Tindakan id)throws MalformedURLException, ParseException;

	void tambahDataMr(Olahmr id)throws MalformedURLException, ParseException;

	List<Diagnosa> getDiagnosaMrByIdOlahMr(int id);

	List<Tindakan> getTindakanMrByIdOlahMr(int id);

	Olahmr getDiagnosaMrByNoRegister(String noRegister);

	Olahmr getDiagnosaTindakanMr(String noRegister, Date tanggalPeriksa, String idDokter);

	boolean UbahDiagnosa(Diagnosa diagnosa);

	boolean UbahTindakan(Tindakan tindakan);

	boolean UbahDataMr(Olahmr olahmr);

	boolean hapusDiagnosa(Diagnosa diagnosa);

	boolean HapusTindakan(Tindakan tindakan);

	void tambahDataMrFinal(Olahmr id)throws MalformedURLException, ParseException;

	boolean UbahDataMrFinal(Olahmr olahmr);

	
	BpjsOlahData getBpjsOlahDataByNoRegister(String noRegister);
	void tambahBpjsOlahData(BpjsOlahData bpjsOlahData);
	void tambahBpjsDiagnosa(BpjsDiagnosa diag);
	void tambahBpjsTindakan(BpjsTindakan tind);
	void ubahBpjsOlahData(BpjsOlahData bpjsOlahData);
	void ubahBpjsDiagnosa(BpjsDiagnosa bpjsDiagnosa);
	void ubahBpjsTindakan(BpjsTindakan bpjsTindakan);
	List<TanggalLibur> getTanggalLibur(String tahun, String bulan);
	List<TanggalLibur> getTanggalLiburByPeriode(String awal, String akhir);
	void tambahTanggalLibur(TanggalLibur tanggalLibur);

	List<Object[]> getOlahMrDanDiagnosaByTanggalHariIni();

	List<Object[]> getOlahMrDanDiagnosaBy(Integer bulan, Integer tahun);

	List<Object[]> getOlahMrDanDiagnosaByTanggalSatuDanDua(Date tanggalSatu, Date tanggalDua);

	Boolean getStatusKunjunganByPasienId(Integer pasienId);
	void tambahStatusKunjungan(StatusKunjungan statusKunjungan);

	boolean cekTanggalLibur(String tanggal);

	
	
}
