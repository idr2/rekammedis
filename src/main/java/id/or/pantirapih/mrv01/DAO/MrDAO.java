package id.or.pantirapih.mrv01.DAO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import id.or.pantirapih.mrv01.Service.StatusCode;
import id.or.pantirapih.mrv01.Exception.MrException;
import id.or.pantirapih.mrv01.Model.BpjsDiagnosa;
import id.or.pantirapih.mrv01.Model.BpjsOlahData;
import id.or.pantirapih.mrv01.Model.BpjsTindakan;
import id.or.pantirapih.mrv01.Model.BpjsWrapper;
import id.or.pantirapih.mrv01.Model.Diagnosa;
import id.or.pantirapih.mrv01.Model.Olahmr;
import id.or.pantirapih.mrv01.Model.StatusKunjungan;
import id.or.pantirapih.mrv01.Model.TanggalLibur;
import id.or.pantirapih.mrv01.Model.Tindakan;

@Transactional
@Repository
public class MrDAO implements IMrDAO {

	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private EntityManagerFactory entityManagerFactory;
//	String Pendaftaran = "http://192.168.11.211:8080/PendaftaranV3/PendaftaranV3";
//	String BPJS = "http://192.168.11.105:8080/Sep";

//	String Pendaftaran = "http://192.168.11.211:8080/PendaftaranVer4/Pendaftaran";
//	String BPJS = "http://192.168.11.105:8080/SepVer4";
	
	//kubernetes
	String Pendaftaran = "http://kubernetes.pantirapih.id/Pendaftaran";
//	String BPJS = "http://192.168.11.105:8080/SepVer4";
	
	
	public org.json.simple.JSONObject parser(String data) throws MalformedURLException, ParseException {
		JSONParser parser = new JSONParser();
		// String a =
		// "[{\"alamats\":[{\"idalamat\":392,\"jalan\":\"jalan\",\"alamat\":\"SATRIAMEKAR\",\"status\":\"2\",\"pasien_id\":45}}]]";
		org.json.simple.JSONObject json = (org.json.simple.JSONObject) parser.parse(data);

		return json;
	}

	public org.json.simple.JSONObject getOtherService(URL urls) throws ParseException, MalformedURLException {
		String output = null;
		try {
			System.out.println("INi url nya = " + urls);
			URL url = urls;
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			// System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				// System.out.println(output);
				return parser(output);
			}
			conn.disconnect();
		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return parser(output);
	}

	@Override
	public void tambahDataDiagnosa(Diagnosa id) throws MalformedURLException, ParseException {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

		entityManager.persist(id);
		System.out.println(new ResponseEntity<Diagnosa>(id, HttpStatus.CREATED));
		new ResponseEntity<Diagnosa>(id, HttpStatus.CREATED);

		try {
			Diagnosa sss = entityManager.merge(id);
			id.setIdDiagnosaOlahMr(sss.getIdDiagnosaOlahMr());
			id.setDeleted("0");
		} catch (Exception e) {
			throw new MrException(e.getMessage(), StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public void tambahDataTindakan(Tindakan id) throws MalformedURLException, ParseException {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

		entityManager.persist(id);
		System.out.println(new ResponseEntity<Tindakan>(id, HttpStatus.CREATED));
		new ResponseEntity<Tindakan>(id, HttpStatus.CREATED);

		try {
			Tindakan sss = entityManager.merge(id);
			id.setIdTindakan(sss.getIdTindakan());
			id.setDeleted("0");
		} catch (Exception e) {
			throw new MrException(e.getMessage(), StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public void tambahDataMr(Olahmr id) throws MalformedURLException, ParseException {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		String status = "0";
		id.setStatus(status);
		id.setTglSave(timeStamp);
		entityManager.persist(id);
		System.out.println(new ResponseEntity<Olahmr>(id, HttpStatus.CREATED));
		new ResponseEntity<Olahmr>(id, HttpStatus.CREATED);

		try {
			Olahmr sss = entityManager.merge(id);
			id.setIdOlahMr(sss.getIdOlahMr());
		} catch (Exception e) {
			throw new MrException(e.getMessage(), StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public List<Diagnosa> getDiagnosaMrByIdOlahMr(int id) {
		String hql = "FROM Diagnosa as atcl WHERE atcl.idOlahMr = ? AND atcl.deleted=0";
		return (List<Diagnosa>) entityManager.createQuery(hql).setParameter(1, id).getResultList();
	}

	@Override
	public List<Tindakan> getTindakanMrByIdOlahMr(int id) {
		String hql = "FROM Tindakan as atcl WHERE atcl.idOlahMr = ? AND atcl.deleted=0";
		return (List<Tindakan>) entityManager.createQuery(hql).setParameter(1, id).getResultList();
	}

	@Override
	public Olahmr getDiagnosaMrByNoRegister(String noRegister) {
		String hql = "FROM Olahmr as atcl WHERE atcl.noRegister = ?";
		return (Olahmr) entityManager.createQuery(hql).setParameter(1, noRegister).getSingleResult();
	}

	@Override
	public Olahmr getDiagnosaTindakanMr(String noRegister, Date tanggalPeriksa, String idDokter) {
		String hql = "FROM Olahmr as atcl WHERE atcl.noRegister = ? AND atcl.tanggalPeriksa = ? AND idDokter = ?";
		return (Olahmr) entityManager.createQuery(hql).setParameter(1, noRegister).setParameter(2, tanggalPeriksa)
				.setParameter(3, idDokter).getSingleResult();
	}

	@Override
	public boolean UbahDiagnosa(Diagnosa diagnosa) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		Diagnosa tampdiagnosa = entityManager.find(Diagnosa.class, diagnosa.getIdDiagnosaOlahMr());

		if (tampdiagnosa != null) {
			tampdiagnosa.setIcd10(diagnosa.getIcd10());
			tampdiagnosa.setKeterangan(diagnosa.getKeterangan());
			tampdiagnosa.setStatus(diagnosa.getStatus());
			tampdiagnosa.setDeleted("0");
			entityManager.flush();
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean UbahTindakan(Tindakan tindakan) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		Tindakan tamptindakan = entityManager.find(Tindakan.class, tindakan.getIdTindakan());

		if (tamptindakan != null) {
			tamptindakan.setKodeTindakan(tindakan.getKodeTindakan());
			tamptindakan.setNamaTindakan(tindakan.getNamaTindakan());
			tamptindakan.setDeleted("0");
			entityManager.flush();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean UbahDataMr(Olahmr olahmr) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		Olahmr tampOlahmr = entityManager.find(Olahmr.class, olahmr.getIdOlahMr());
		if (tampOlahmr != null) {
			tampOlahmr.setNoRegister(olahmr.getNoRegister());
			entityManager.flush();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean UbahDataMrFinal(Olahmr olahmr) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		Olahmr tampOlahmr = entityManager.find(Olahmr.class, olahmr.getIdOlahMr());
		if (tampOlahmr != null) {
			tampOlahmr.setNoRegister(olahmr.getNoRegister());
			tampOlahmr.setStatus("1");
			entityManager.flush();
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean hapusDiagnosa(Diagnosa diagnosa) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		Diagnosa tampdiagnosa = entityManager.find(Diagnosa.class, diagnosa.getIdDiagnosaOlahMr());

		if (tampdiagnosa != null) {
			tampdiagnosa.setDeleted("1");
			entityManager.flush();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean HapusTindakan(Tindakan tindakan) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		Tindakan tamptindakan = entityManager.find(Tindakan.class, tindakan.getIdTindakan());

		if (tamptindakan != null) {
			tamptindakan.setDeleted("1");
			entityManager.flush();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void tambahDataMrFinal(Olahmr id) throws MalformedURLException, ParseException {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		String status = "1";
		id.setStatus(status);
		id.setTglSave(timeStamp);
		entityManager.persist(id);
		System.out.println(new ResponseEntity<Olahmr>(id, HttpStatus.CREATED));
		new ResponseEntity<Olahmr>(id, HttpStatus.CREATED);

		try {
			Olahmr sss = entityManager.merge(id);
			id.setIdOlahMr(sss.getIdOlahMr());
		} catch (Exception e) {
			throw new MrException(e.getMessage(), StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public BpjsOlahData getBpjsOlahDataByNoRegister(String noRegister) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String hql = "FROM BpjsOlahData as atcl WHERE atcl.deleted = 'False' AND atcl.noRegister = ?";
		return (BpjsOlahData) entityManager.createQuery(hql).setParameter(1, noRegister).getSingleResult();
	}

	@Override
	public void tambahBpjsOlahData(BpjsOlahData bpjsOlahData) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		BpjsOlahData as = entityManager.merge(bpjsOlahData);
		bpjsOlahData.setIdBpjsOlahData(as.getIdBpjsOlahData());
	}

	@Override
	public void tambahBpjsDiagnosa(BpjsDiagnosa diag) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		diag.setAddDate(timeStamp);
		diag.setUpdateDate(timeStamp);
		diag.setDeleted(false);
		BpjsDiagnosa as = entityManager.merge(diag);
		diag.setIdBpjsDiagnosa(as.getIdBpjsDiagnosa());
	}

	@Override
	public void tambahBpjsTindakan(BpjsTindakan tind) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		tind.setAddDate(timeStamp);
		tind.setUpdateDate(timeStamp);
		tind.setDeleted(false);
		BpjsTindakan as = entityManager.merge(tind);
		tind.setIdBpjsTindakan(as.getIdBpjsTindakan());
	}

	@Override
	public void ubahBpjsOlahData(BpjsOlahData bpjsOlahData) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		BpjsOlahData olahData = getBpjsOlahDataById(bpjsOlahData.getIdBpjsOlahData());
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		olahData.setDokterId(bpjsOlahData.getDokterId());
		olahData.setUpdateDate(timeStamp);
		olahData.setTanggalPeriksa(bpjsOlahData.getTanggalPeriksa());
		olahData.setDeleted(bpjsOlahData.getDeleted());

		entityManager.flush();
	}

	private BpjsOlahData getBpjsOlahDataById(Long idBpjsOlahData) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		return (BpjsOlahData) entityManager.find(BpjsOlahData.class, idBpjsOlahData);
	}

	@Override
	public void ubahBpjsDiagnosa(BpjsDiagnosa bpjsDiagnosa) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		BpjsDiagnosa diag = getBpjsDiagnosaById(bpjsDiagnosa.getIdBpjsDiagnosa());
		diag.setDeleted(bpjsDiagnosa.getDeleted());
		diag.setKodeIcd(bpjsDiagnosa.getKodeIcd());
		diag.setNamaIcd(bpjsDiagnosa.getNamaIcd());
		diag.setStatus(bpjsDiagnosa.getStatus());
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		diag.setUpdateDate(timeStamp);

		entityManager.flush();
	}

	private BpjsDiagnosa getBpjsDiagnosaById(Long idBpjsDiagnosa) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		return (BpjsDiagnosa) entityManager.find(BpjsDiagnosa.class, idBpjsDiagnosa);
	}

	@Override
	public void ubahBpjsTindakan(BpjsTindakan bpjsTindakan) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		BpjsTindakan tind = getBpjsTindakanById(bpjsTindakan.getIdBpjsTindakan());
		tind.setDeleted(bpjsTindakan.getDeleted());
		tind.setKodeTindakan(bpjsTindakan.getKodeTindakan());
		tind.setNamaTindakan(bpjsTindakan.getNamaTindakan());
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		tind.setUpdateDate(timeStamp);

		entityManager.flush();
	}

	private BpjsTindakan getBpjsTindakanById(Long idBpjsTindakan) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		return (BpjsTindakan) entityManager.find(BpjsTindakan.class, idBpjsTindakan);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TanggalLibur> getTanggalLibur(String tahun, String bulan) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String hql = "FROM TanggalLibur as atcl WHERE atcl.tahun = ? AND atcl.bulan = ?";
		return (List<TanggalLibur>) entityManager.createQuery(hql).setParameter(1, tahun).setParameter(2, bulan)
				.getResultList();
	}

	@Override
	public void tambahTanggalLibur(TanggalLibur tanggalLibur) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		tanggalLibur.setAddDate(timeStamp);
		tanggalLibur.setStatus(false);
		TanggalLibur as = entityManager.merge(tanggalLibur);
		tanggalLibur.setIdTanggalLibur(as.getIdTanggalLibur());
	}
	
	@Override
	public boolean cekTanggalLibur(String tanggal) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String hql = "FROM TanggalLibur as atcl WHERE atcl.tanggal = '"+tanggal+"'";
		TanggalLibur tamp = (TanggalLibur) entityManager.createQuery(hql).getSingleResult();
		if(tamp != null) {
			return true;
		}
		else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TanggalLibur> getTanggalLiburByPeriode(String awal, String akhir) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String hql = "FROM TanggalLibur as atcl WHERE atcl.tanggal >= ? AND atcl.tanggal <= ?";
		return (List<TanggalLibur>) entityManager.createQuery(hql).setParameter(1, awal).setParameter(2, akhir)
				.getResultList();
	}

	@Override
	public List<Object[]> getOlahMrDanDiagnosaByTanggalHariIni() {
		String hql = "SELECT atcl.idOlahMr,"
				+ " atcl.idKunjungan,"
				+ " atcl.noRegister,"
				+ " atcl.noMr,"
				+ " atcl.namaPasien,"
				+ " atcl.tanggalPeriksa,"
				+ " atcl.idDokter FROM Olahmr as atcl WHERE atcl.tanggalPeriksa = CURDATE()";
		return (List<Object[]>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public List<Object[]> getOlahMrDanDiagnosaBy(Integer bulan, Integer tahun) {
		String hql = "SELECT atcl.idOlahMr,"
				+ " atcl.idKunjungan,"
				+ " atcl.noRegister,"
				+ " atcl.noMr,"
				+ " atcl.namaPasien,"
				+ " atcl.tanggalPeriksa,"
				+ " atcl.idDokter FROM Olahmr as atcl WHERE EXTRACT(MONTH FROM atcl.tanggalPeriksa)= "+bulan+" AND EXTRACT(YEAR FROM atcl.tanggalPeriksa)= "+tahun ;
		return (List<Object[]>) entityManager.createQuery(hql).getResultList();
	}
	
	@Override
	public List<Object[]> getOlahMrDanDiagnosaByTanggalSatuDanDua(Date tanggalSatu, Date tanggalDua) {
		String hql = "SELECT atcl.idOlahMr,"
				+ " atcl.idKunjungan,"
				+ " atcl.noRegister,"
				+ " atcl.noMr,"
				+ " atcl.namaPasien,"
				+ " atcl.tanggalPeriksa,"
				+ " atcl.idDokter FROM Olahmr as atcl WHERE atcl.tanggalPeriksa BETWEEN '"+tanggalSatu+"' AND '"+ tanggalDua+"'";
		return (List<Object[]>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Boolean getStatusKunjunganByPasienId(Integer pasienId) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String hql = "FROM StatusKunjungan as atcl WHERE atcl.pasienId = ?";
		try {
			entityManager.createQuery(hql).setParameter(1, pasienId).getSingleResult();
			return true;
		}catch(Exception e) {
			return false;
		}
	}

	@Override
	public void tambahStatusKunjungan(StatusKunjungan statusKunjungan) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		statusKunjungan.setAddDate(timeStamp);
		StatusKunjungan tamp = entityManager.merge(statusKunjungan);
		statusKunjungan.setIdStatusKunjungan(tamp.getIdStatusKunjungan());
	}

}
