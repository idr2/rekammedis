package id.or.pantirapih.mrv01.Exception;

import java.util.Arrays;
import java.util.List;

public class MrException extends RuntimeException{
	 private Integer errorCode;
	 private List<String> errorMessage;
	 
	    public MrException(String errorCause, List<String> errorMessage, Integer errorCode) {
	        super(errorCause);
	        this.errorCode = errorCode;
	        this.errorMessage = errorMessage;
	    }
	    
	    public MrException(String errorCause, String errorMessage, Integer errorCode) {
	        super(errorCause);
	        this.errorCode = errorCode;
	        this.errorMessage = Arrays.asList(errorMessage);
	    }
	    
	    public MrException(String errorCause, Integer errorCode) {
	        super(errorCause);
	        this.errorCode = errorCode;
	        this.errorMessage = null;
	    }
	    
	    public MrException(Integer errorCode, List<String> errorMessage) {
	        this.errorCode = errorCode;
	        this.errorMessage = errorMessage;
	    }
	    
	    public MrException(Integer errorCode, String errorMessage) {
	        this.errorCode = errorCode;
	        this.errorMessage = Arrays.asList(errorMessage);
	    }
	    
	    
	    
	    public Integer getErrorCode() {
			return errorCode;
		}

		public List<String> getErrorMessage() {
			return errorMessage;
		}
	    

}
