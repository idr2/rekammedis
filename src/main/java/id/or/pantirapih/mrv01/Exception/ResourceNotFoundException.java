package id.or.pantirapih.mrv01.Exception;

public class ResourceNotFoundException extends RuntimeException {
	 private Long resourceId;
	    public ResourceNotFoundException(Long resourceId, String message) {
	        super(message);
	        this.resourceId = resourceId;
	    }
}
