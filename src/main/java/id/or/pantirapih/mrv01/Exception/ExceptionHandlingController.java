package id.or.pantirapih.mrv01.Exception;


import java.sql.SQLException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ExceptionHandlingController {
	
	@ExceptionHandler(MrException.class)
    public ResponseEntity<ExceptionResponse> resourceNotFound(MrException ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode(ex.getErrorCode());
        String messsage = String.join(",", ex.getErrorMessage());
        response.setErrorMessage(messsage);
        response.setErrorCause(ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.OK);
    }
	
	@ExceptionHandler(NoHandlerFoundException.class)	
	public ResponseEntity<ExceptionResponse> noHandlerFoundException(Exception ex) {
		ExceptionResponse response = new ExceptionResponse();
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.OK);
	}
}
