package id.or.pantirapih.mrv01.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import id.or.pantirapih.mrv01.Exception.MrException;
import id.or.pantirapih.mrv01.Model.Dataolahmr;
import id.or.pantirapih.mrv01.Model.Diagnosa;
import id.or.pantirapih.mrv01.Model.Olahmr;
import id.or.pantirapih.mrv01.Model.Tindakan;
import id.or.pantirapih.mrv01.Service.IMrService;
import id.or.pantirapih.mrv01.Service.StatusCode;

@RestController
@RequestMapping("/RekamMedis")
public class MrController {
	@Autowired
	private IMrService iMrService;

	//url develop
//	String pendaftaranPasien = "http://192.168.11.211:8080/PendaftaranV3/PendaftaranV3";
//	String poliLukas = "http://192.168.11.184:8080/PoliLukas";

	//url deploy
//	 String pendaftaranPasien = "http://192.168.11.211:8080/PendaftaranVer4/Pendaftaran";
//	 String poliLukas = "http://192.168.11.184:8080/PoliLukasVer4";
	 
	 //url Kubernetes
	 String pendaftaranPasien = "http://kubernetes.pantirapih.id/Pendaftaran";
	 String poliLukas = "http://kubernetes.pantirapih.id/";

	@GetMapping("getDiagnosaMrByNoRegister/{noRegister}")
	public JSONObject getDiagnosaMrByNoRegister(@PathVariable("noRegister") String noRegister) {
		JSONObject getDiagnosaMr = new JSONObject();
		try {
			Olahmr tampOlahmMr = iMrService.getDiagnosaMrByNoRegister(noRegister);
			int id = tampOlahmMr.getIdOlahMr();
			List<Diagnosa> diagnosa = iMrService.getDiagnosaMrByIdOlahMr(id);
			String statuss = tampOlahmMr.getStatus();

			if (statuss == "0") {
				String statusbc = "Coding";
				getDiagnosaMr.put("statusData", statusbc);
				getDiagnosaMr.put("OlahMr", tampOlahmMr);
				getDiagnosaMr.put("Diagnosa", diagnosa);
			} else {
				String statusc = "Final di Coding";
				getDiagnosaMr.put("statusData", statusc);
				getDiagnosaMr.put("OlahMr", tampOlahmMr);
				getDiagnosaMr.put("Diagnosa", diagnosa);

			}
			return getDiagnosaMr;
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}

	}

	@GetMapping("getDiagnosaTindakanMr/{noRegister}/{tanggalPeriksa}/{idDokter}")
	public JSONObject getDiagnosaTindakanMr(@PathVariable("noRegister") String noRegister,
			@PathVariable("tanggalPeriksa") Date tanggalPeriksa, @PathVariable("idDokter") String idDokter)
			throws MalformedURLException, ParseException {
		JSONObject getDiagnosaMr = new JSONObject();

		try {
			Olahmr tampMr = iMrService.getDiagnosaMrByNoRegister(noRegister);

			Olahmr tampOlahmMr = iMrService.getDiagnosaTindakanMr(noRegister, tanggalPeriksa, idDokter);
			int id = tampOlahmMr.getIdOlahMr();
			String statuss = tampOlahmMr.getStatus();

			List<Diagnosa> diagnosa = iMrService.getDiagnosaMrByIdOlahMr(id);

			try {
				List<Tindakan> tindakan = iMrService.getTindakanMrByIdOlahMr(id);
				if (statuss.matches("0")) {
					String statusbc = "Coding";
					getDiagnosaMr.put("statusData", statusbc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
					getDiagnosaMr.put("Tindakan", tindakan);
				} else {
					String statusc = "Final di Coding";
					getDiagnosaMr.put("statusData", statusc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
					getDiagnosaMr.put("Tindakan", tindakan);
				}
			} catch (Exception e) {
				if (statuss.matches("0")) {
					String statusbc = "Coding";
					getDiagnosaMr.put("statusData", statusbc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
				} else {
					String statusc = "Final di Coding";
					getDiagnosaMr.put("statusData", statusc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
				}
			}

		} catch (Exception e) {

			URL urlpendaftaran = new URL(pendaftaranPasien + "/getKunjunganByNoRegisterRalan/" + noRegister);
			org.json.simple.JSONObject pendaftaran = getOtherService(urlpendaftaran);
			org.json.simple.JSONObject kunjungan = (org.json.simple.JSONObject) pendaftaran.get("kunjungan");

			JSONArray diagnosaa = (JSONArray) kunjungan.get("diagnosa");
			getDiagnosaMr.put("Diagnosa", diagnosaa);
		}
		return getDiagnosaMr;
	}

	@GetMapping("getDiagnosaTindakanMrLukas/{noRegister}/{tanggalPeriksa}/{idDokter}")
	public JSONObject getDiagnosaTindakanMrLukas(@PathVariable("noRegister") String noRegister,
			@PathVariable("tanggalPeriksa") Date tanggalPeriksa, @PathVariable("idDokter") String idDokter)
			throws MalformedURLException, ParseException {
		JSONObject getDiagnosaMr = new JSONObject();

		try {
			Olahmr tampMr = iMrService.getDiagnosaMrByNoRegister(noRegister);

			Olahmr tampOlahmMr = iMrService.getDiagnosaTindakanMr(noRegister, tanggalPeriksa, idDokter);
			int id = tampOlahmMr.getIdOlahMr();
			String statuss = tampOlahmMr.getStatus();

			List<Diagnosa> diagnosa = iMrService.getDiagnosaMrByIdOlahMr(id);

			try {
				List<Tindakan> tindakan = iMrService.getTindakanMrByIdOlahMr(id);
				if (statuss.matches("0")) {
					String statusbc = "Coding";
					getDiagnosaMr.put("statusData", statusbc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
					getDiagnosaMr.put("Tindakan", tindakan);
				} else {
					String statusc = "Final di Coding";
					getDiagnosaMr.put("statusData", statusc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
					getDiagnosaMr.put("Tindakan", tindakan);
				}
			} catch (Exception e) {
				if (statuss.matches("0")) {
					String statusbc = "Coding";
					getDiagnosaMr.put("statusData", statusbc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
				} else {
					String statusc = "Final di Coding";
					getDiagnosaMr.put("statusData", statusc);
					getDiagnosaMr.put("OlahMr", tampOlahmMr);
					getDiagnosaMr.put("Diagnosa", diagnosa);
				}
			}

		} catch (Exception e) {

			URL urlLukas = new URL(poliLukas + "/PoliLukas/getPendaftaranLukasByNoRegisterRalan/" + noRegister);
			org.json.simple.JSONObject polilukas = getOtherService(urlLukas);

			org.json.simple.JSONObject kunjungan = (org.json.simple.JSONObject) polilukas.get("kunjunganLukas");

			JSONArray diagnosaa = (JSONArray) kunjungan.get("diagnosa");
			getDiagnosaMr.put("Diagnosa", diagnosaa);
		}
		return getDiagnosaMr;
	}

	@GetMapping("getOlahMrDanDiagnosaByTanggalHariIni")
	public JSONArray getOlahMrDanDiagnosaByTanggalHariIni() {
		List<Object[]> tampOlahMr = iMrService.getOlahMrDanDiagnosaByTanggalHariIni();
		JSONArray array = new JSONArray();
		for (Object[] result : tampOlahMr) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("idOlahMr", result[0]);
			Integer idOlahMr = (Integer) result[0];

			try {
				List<Diagnosa> diagnosa = iMrService.getDiagnosaMrByIdOlahMr(idOlahMr);
				jsonObj.put("idKunjungan", result[1]);
				jsonObj.put("noRegister", result[2]);
				jsonObj.put("noMr", result[3]);
				jsonObj.put("namaPasien", result[4]);
				jsonObj.put("tanggalPeriksa", result[5]);
				jsonObj.put("idDokter", result[6]);
				jsonObj.put("Diagnosa", diagnosa);
				array.add(jsonObj);
			} catch (Exception e) {
				jsonObj.put("idKunjungan", result[1]);
				jsonObj.put("noRegister", result[2]);
				jsonObj.put("noMr", result[3]);
				jsonObj.put("namaPasien", result[4]);
				jsonObj.put("tanggalPeriksa", result[5]);
				jsonObj.put("idDokter", result[6]);
				array.add(jsonObj);
			}
		}
		return array;
	}

	@GetMapping("getOlahMrDanDiagnosaBy/{bulan}/{tahun}")
	public JSONArray getOlahMrDanDiagnosaBy(@PathVariable("bulan") Integer bulan, @PathVariable("tahun") Integer tahun) {
		List<Object[]> tampOlahMr = iMrService.getOlahMrDanDiagnosaBy(bulan, tahun);
		JSONArray array = new JSONArray();
		for (Object[] result : tampOlahMr) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("idOlahMr", result[0]);
			Integer idOlahMr = (Integer) result[0];

			try {
				List<Diagnosa> diagnosa = iMrService.getDiagnosaMrByIdOlahMr(idOlahMr);
				jsonObj.put("idKunjungan", result[1]);
				jsonObj.put("noRegister", result[2]);
				jsonObj.put("noMr", result[3]);
				jsonObj.put("namaPasien", result[4]);
				jsonObj.put("tanggalPeriksa", result[5]);
				jsonObj.put("idDokter", result[6]);
				jsonObj.put("Diagnosa", diagnosa);
				array.add(jsonObj);
			} catch (Exception e) {
				jsonObj.put("idKunjungan", result[1]);
				jsonObj.put("noRegister", result[2]);
				jsonObj.put("noMr", result[3]);
				jsonObj.put("namaPasien", result[4]);
				jsonObj.put("tanggalPeriksa", result[5]);
				jsonObj.put("idDokter", result[6]);
				array.add(jsonObj);
			}
		}
		return array;
	}

	@GetMapping("getOlahMrDanDiagnosaByTanggalSatuDanDua/{tanggalSatu}/{tanggalDua}")
	public JSONArray getOlahMrDanDiagnosaByTanggalSatuDanDua(@PathVariable("tanggalSatu") Date tanggalSatu,
			@PathVariable("tanggalDua") Date tanggalDua) {
		List<Object[]> tampOlahMr = iMrService.getOlahMrDanDiagnosaByTanggalSatuDanDua(tanggalSatu, tanggalDua);
		JSONArray array = new JSONArray();
		for (Object[] result : tampOlahMr) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("idOlahMr", result[0]);
			Integer idOlahMr = (Integer) result[0];

			try {
				List<Diagnosa> diagnosa = iMrService.getDiagnosaMrByIdOlahMr(idOlahMr);
				jsonObj.put("idKunjungan", result[1]);
				jsonObj.put("noRegister", result[2]);
				jsonObj.put("noMr", result[3]);
				jsonObj.put("namaPasien", result[4]);
				jsonObj.put("tanggalPeriksa", result[5]);
				jsonObj.put("idDokter", result[6]);
				jsonObj.put("Diagnosa", diagnosa);
				array.add(jsonObj);
			} catch (Exception e) {
				jsonObj.put("idKunjungan", result[1]);
				jsonObj.put("noRegister", result[2]);
				jsonObj.put("noMr", result[3]);
				jsonObj.put("namaPasien", result[4]);
				jsonObj.put("tanggalPeriksa", result[5]);
				jsonObj.put("idDokter", result[6]);
				array.add(jsonObj);
			}
		}
		return array;
	}

	@GetMapping("getAllKunjunganDanStatus/{iddokter}/{tanggalperiksa}/{idklinik}/{rangewaktu}")
	public JSONArray getAllKunjunganDanStatus(@PathVariable("iddokter") String iddokter,
			@PathVariable("tanggalperiksa") Date tanggalperiksa, @PathVariable("idklinik") String idklinik,
			@PathVariable("rangewaktu") String rangewaktu) throws MalformedURLException, ParseException {
		//JSONObject getDiagnosaMr = new JSONObject();
		JSONArray array = new JSONArray();

		try {
			URL urlPendaftaran = new URL(pendaftaranPasien + "/getKunjunganByIdDokterTanggalIdKlinikWaktu/" + iddokter + "/"
					+ tanggalperiksa + "/" + idklinik + "/" + rangewaktu);
			JSONArray tampPendaftaran = getOtherServiceArray(urlPendaftaran);
			for (int i = 0; i < tampPendaftaran.size()-1; i++) {
				
				org.json.simple.JSONObject combined = new org.json.simple.JSONObject();
				JSONObject hasilKunjungan = (JSONObject) tampPendaftaran.get(i);
				org.json.simple.JSONObject kunjungans = (org.json.simple.JSONObject) hasilKunjungan.get("kunjungan");
				String noRegister = kunjungans.get("noRegisterRalan").toString();

				try {
					Olahmr tampMr = iMrService.getDiagnosaMrByNoRegister(noRegister);
					String statuss = tampMr.getStatus();
					combined.put("Kunjungan", hasilKunjungan);
					combined.put("Status", statuss);
					array.add(combined);
				} catch (Exception e) {
					String Statusnya = "3";
					combined.put("Kunjungan", hasilKunjungan);
					combined.put("Status", Statusnya);

					array.add(combined);

				}
			}
			return array;

		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}

	}

	@GetMapping("getAllKunjunganDanStatusLukas/{tanggalperiksa}/{iddokter}/{idklinik}")
	public JSONArray getAllKunjunganDanStatusLukas(@PathVariable("tanggalperiksa") Date tanggalperiksa,
			@PathVariable("iddokter") String iddokter, @PathVariable("idklinik") String idklinik)
			throws MalformedURLException, ParseException {
		//JSONObject getDiagnosaMr = new JSONObject();
		JSONArray array = new JSONArray();

		try {
			URL urlPendaftaran = new URL(
					poliLukas + "/PoliLukas/getSemuaPendaftaranLukasByTanggalPeriksaDanIdDokterDanIdKlinik/" + tanggalperiksa
							+ "/" + iddokter + "/" + idklinik);
			JSONArray tampPendaftaran = getOtherServiceArray(urlPendaftaran);
			Integer aaa = tampPendaftaran.size() - 1;
			for (int i = 0; i <= aaa; i++) {
				org.json.simple.JSONObject combined = new org.json.simple.JSONObject();
				JSONObject hasilKunjungan = (JSONObject) tampPendaftaran.get(i);
				org.json.simple.JSONObject kunjungans = (org.json.simple.JSONObject) hasilKunjungan.get("kunjunganLukas");
				String noRegister = kunjungans.get("noRegisterRalan").toString();

				try {
					Olahmr tampMr = iMrService.getDiagnosaMrByNoRegister(noRegister);
					String statuss = tampMr.getStatus();
					combined.put("Kunjungan", hasilKunjungan);
					combined.put("Status", statuss);
					array.add(combined);
				} catch (Exception e) {
					String Statusnya = "3";
					combined.put("Kunjungan", hasilKunjungan);
					combined.put("Status", Statusnya);
					array.add(combined);
				}
			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		return array;
	}

	@GetMapping("getDiagnosaMrByIdOlahMr/{id}")
	public ResponseEntity<List<Diagnosa>> getDiagnosaMrByIdOlahMr(@PathVariable("id") int id) {
		List<Diagnosa> DiagnosaMr = iMrService.getDiagnosaMrByIdOlahMr(id);
		return new ResponseEntity<List<Diagnosa>>(DiagnosaMr, HttpStatus.OK);
	}

	@GetMapping("getTindakanMrByIdOlahMr/{id}")
	public ResponseEntity<List<Tindakan>> getTindakanMrByIdOlahMr(@PathVariable("id") int id) {
		List<Tindakan> TindakanMr = iMrService.getTindakanMrByIdOlahMr(id);
		return new ResponseEntity<List<Tindakan>>(TindakanMr, HttpStatus.OK);
	}

	@RequestMapping(value = "/TambahDiagnosa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> tambahDataDiagnosa(@RequestBody Diagnosa id, UriComponentsBuilder builder)
			throws MalformedURLException, ParseException {
		boolean flag = iMrService.tambahDataDiagnosa(id);

		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/TambahTindakan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> tambahDataTindakan(@RequestBody Tindakan id, UriComponentsBuilder builder)
			throws MalformedURLException, ParseException {
		boolean flag = iMrService.tambahDataTindakan(id);

		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/TambahDataMr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> tambahDataMr(@RequestBody Olahmr id, UriComponentsBuilder builder)
			throws MalformedURLException, ParseException {
		boolean flag = iMrService.tambahDataMr(id);

		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/TambahDataMrFinal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> tambahDataMrFinal(@RequestBody Olahmr id, UriComponentsBuilder builder)
			throws MalformedURLException, ParseException {
		boolean flag = iMrService.tambahDataMrFinal(id);

		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/TambahSemuaDataMr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public <T> ResponseEntity<Void> TambahSemuaDataMr(@RequestBody Dataolahmr dataolahmr, UriComponentsBuilder builder) {

		iMrService.TambahSemuaDataMr(dataolahmr);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/TambahSemuaDataMrFinal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public <T> ResponseEntity<Void> TambahSemuaDataMrFinal(@RequestBody Dataolahmr dataolahmr,
			UriComponentsBuilder builder) {

		iMrService.TambahSemuaDataMrFinal(dataolahmr);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/UbahDiagnosa", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> UbahDiagnosa(@RequestBody Diagnosa diagnosa) {
		boolean flag = iMrService.UbahDiagnosa(diagnosa);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/UbahTindakan", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> UbahTindakan(@RequestBody Tindakan tindakan) {
		boolean flag = iMrService.UbahTindakan(tindakan);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/UbahDataMr", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> UbahDataMr(@RequestBody Olahmr olahmr) {
		boolean flag = iMrService.UbahDataMr(olahmr);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/UbahDataMrFinal", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> UbahDataMrFinal(@RequestBody Olahmr olahmr) {
		boolean flag = iMrService.UbahDataMrFinal(olahmr);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/UbahSemuaDataMr", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public <T> ResponseEntity<Void> UbahSemuaDataMr(@RequestBody Dataolahmr dataolahmr, UriComponentsBuilder builder) {

		iMrService.UbahSemuaDataMr(dataolahmr);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/UbahSemuaDataMrFinal", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public <T> ResponseEntity<Void> UbahSemuaDataMrFinal(@RequestBody Dataolahmr dataolahmr,
			UriComponentsBuilder builder) {

		iMrService.UbahSemuaDataMrFinal(dataolahmr);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/HapusDiagnosa", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> hapusDiagnosa(@RequestBody Diagnosa diagnosa) {
		boolean flag = iMrService.hapusDiagnosa(diagnosa);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/HapusTindakan", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> HapusTindakan(@RequestBody Tindakan tindakan) {
		boolean flag = iMrService.HapusTindakan(tindakan);
		if (flag == false) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	public org.json.simple.JSONObject parser(String data)
			throws MalformedURLException, ParseException, org.json.simple.parser.ParseException {
		JSONParser parser = new JSONParser();
		//String a = "[{\"alamats\":[{\"idalamat\":392,\"jalan\":\"jalan\",\"alamat\":\"SATRIAMEKAR\",\"status\":\"2\",\"pasien_id\":45}}]]";
		org.json.simple.JSONObject json = (org.json.simple.JSONObject) parser.parse(data);

		return json;
	}

	public org.json.simple.JSONObject getOtherService(URL urls)
			throws ParseException, MalformedURLException, org.json.simple.parser.ParseException {
		String output = null;
		try {
			System.out.println("INi url nya = " + urls);
			URL url = urls;
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			//System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
				return parser(output);
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return parser(output);
	}

	public JSONArray parserArray(String data) throws MalformedURLException, ParseException {
		JSONParser parser = new JSONParser();
		// String a =
		// "[{\"alamats\":[{\"idalamat\":392,\"jalan\":\"jalan\",\"alamat\":\"SATRIAMEKAR\",\"status\":\"2\",\"pasien_id\":45}}]]";
		JSONArray json = (JSONArray) parser.parse(data);

		return json;
	}

	public JSONArray getOtherServiceArray(URL urls) throws ParseException, MalformedURLException {
		String output = null;
		try {
			System.out.println("INi url nya = " + urls);
			URL url = urls;
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			// System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				// System.out.println(output);
				return parserArray(output);
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return parserArray(output);
	}

}
