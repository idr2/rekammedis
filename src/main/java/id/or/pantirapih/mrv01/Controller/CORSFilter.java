package id.or.pantirapih.mrv01.Controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.GenericFilterBean;

public class CORSFilter extends GenericFilterBean implements Filter{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletResponse httpresponse = (HttpServletResponse) response;
		
		httpresponse.setHeader("Access-Control-Allow-Origin", "*");
		httpresponse.setHeader("Access-Allow-Method", "*");
		httpresponse.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		httpresponse.setHeader("Access-Allow-Method", "*");
		httpresponse.setHeader("Access-Control-Allow-Credentials", "*");
		httpresponse.setHeader("Access-Control-Allow-Max-Age", "3600");
		httpresponse.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS");


		chain.doFilter(request, response); 	  	
		
	}
}
