package id.or.pantirapih.mrv01.Controller;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import id.or.pantirapih.mrv01.Model.BpjsDiagnosa;
import id.or.pantirapih.mrv01.Model.BpjsOlahData;
import id.or.pantirapih.mrv01.Model.BpjsTindakan;
import id.or.pantirapih.mrv01.Model.BpjsWrapper;
import id.or.pantirapih.mrv01.Service.IMrService;

@RestController
@RequestMapping("/BpjsOlahData")
public class BpjsOlahDataController {
	
	@Autowired
	private IMrService iMrService;
	
	@RequestMapping(value = "/getBpjsOlahDataByNoRegister/{noRegister}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject getBpjsOlahDataByNoRegister(@PathVariable("noRegister") String noRegister) {
		JSONObject tamp = iMrService.getBpjsOlahDataByNoRegister(noRegister);
		return tamp;
	}
	
	@RequestMapping(value = "/tambahBpjsOlahData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public synchronized ResponseEntity<BpjsWrapper> tambahKajianTerpilih(@RequestBody BpjsWrapper bpjsWrapper) {
		iMrService.tambahBpjsOlahData(bpjsWrapper);
		return new ResponseEntity<BpjsWrapper>(bpjsWrapper,HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/ubahBpjsOlahData", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public synchronized ResponseEntity<BpjsOlahData> ubahBpjsOlahData(@RequestBody BpjsOlahData bpjsOlahData, UriComponentsBuilder builder){
		iMrService.ubahBpjsOlahData(bpjsOlahData);
		return new ResponseEntity<BpjsOlahData>(bpjsOlahData,HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/ubahBpjsDiagnosa", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public synchronized ResponseEntity<BpjsDiagnosa> ubahBpjsDiagnosa(@RequestBody BpjsDiagnosa bpjsDiagnosa, UriComponentsBuilder builder){
		iMrService.ubahBpjsDiagnosa(bpjsDiagnosa);
		return new ResponseEntity<BpjsDiagnosa>(bpjsDiagnosa,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ubahBpjsTindakan", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public synchronized ResponseEntity<BpjsTindakan> ubahBpjsTindakan(@RequestBody BpjsTindakan bpjsTindakan, UriComponentsBuilder builder){
		iMrService.ubahBpjsTindakan(bpjsTindakan);
		return new ResponseEntity<BpjsTindakan>(bpjsTindakan,HttpStatus.OK);
	}	
}
