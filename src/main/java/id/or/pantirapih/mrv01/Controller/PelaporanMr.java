package id.or.pantirapih.mrv01.Controller;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import id.or.pantirapih.mrv01.Model.DataKasus;
import id.or.pantirapih.mrv01.Model.DataKunjungan;
import id.or.pantirapih.mrv01.Model.DataPasien;
import id.or.pantirapih.mrv01.Model.DataPembayaran;
import id.or.pantirapih.mrv01.Model.StatusKunjungan;
import id.or.pantirapih.mrv01.Model.TanggalLibur;
import id.or.pantirapih.mrv01.Model.WrapperPenggabunganRm;
import id.or.pantirapih.mrv01.Service.IMrService;

@RestController
@RequestMapping("/PelaporanMr")
public class PelaporanMr {

	@Autowired
	private IMrService iMrService;

	/*-----------------------------------------------------Pelaporan Data Pasien-----------------------------------------------------------*/

	@RequestMapping(value = "/getDataPasienHariIni", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public DataPasien getDataPasienHariIni() {
		DataPasien tamp = iMrService.getDataPasienHariIni();
		return tamp;
	}

	@RequestMapping(value = "/getDataPasienByBulan/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataPasien> getDataPasienByBulan(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		List<DataPasien> tamp = iMrService.getDataPasienByBulan(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getDataPasienByBulanTersortir/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject getDataPasienByBulanTersortir(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		JSONObject tamp = iMrService.getDataPasienByBulanTersortir(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getDataPasienByPeriodeTersortir/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject getDataPasienByPeriodeTersortir(@PathVariable("awal") String awal,
			@PathVariable("akhir") String akhir) {
		JSONObject tamp = iMrService.getDataPasienByPeriodeTersortir(awal, akhir);
		return tamp;
	}

	/*-----------------------------------------------------Tanggal Libur-----------------------------------------------------------*/

	@RequestMapping(value = "/getTanggalLibur/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TanggalLibur> getTanggalLibur(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		List<TanggalLibur> tamp = iMrService.getTanggalLibur(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getTanggalLiburByPeriode/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TanggalLibur> getTanggalLiburByPeriode(@PathVariable("awal") String awal,
			@PathVariable("akhir") String akhir) {
		List<TanggalLibur> tamp = iMrService.getTanggalLiburByPeriode(awal, akhir);
		return tamp;
	}
	
	@RequestMapping(value = "/cekTanggalLibur/{tanggal}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject cekTanggalLibur(@PathVariable("tanggal") String tanggal) {
		boolean statusLibur= iMrService.cekTanggalLibur(tanggal);
		JSONObject tamp = new JSONObject();
		tamp.put("status", statusLibur);
		return tamp;
	}
	
	@RequestMapping(value = "/tambahTanggalLibur", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public synchronized ResponseEntity<TanggalLibur> tambahTanggalLibur(@RequestBody TanggalLibur tanggalLibur,
			UriComponentsBuilder builder) {
		iMrService.tambahTanggalLibur(tanggalLibur);
		return new ResponseEntity<TanggalLibur>(tanggalLibur, HttpStatus.CREATED);
	}

	/*-----------------------------------------------------Pelaporan Data Kasus-----------------------------------------------------------*/

	@RequestMapping(value = "/getKasusHariIni", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public DataKasus getKasusHariIni() {
		DataKasus tamp = iMrService.getKasusHariIni();
		return tamp;
	}

	@RequestMapping(value = "/getKasusByBulan/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKasus> getKasusByBulan(@PathVariable("tahun") String tahun, @PathVariable("bulan") String bulan) {
		List<DataKasus> tamp = iMrService.getKasusByBulan(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getKasusByPeriode/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKasus> getKasusByPeriode(@PathVariable("awal") String awal, @PathVariable("akhir") String akhir) {
		List<DataKasus> tamp = iMrService.getKasusByPeriode(awal, akhir);
		return tamp;
	}

	/*-----------------------------------------------------Pelaporan Data Pembayaran-----------------------------------------------------------*/

	@RequestMapping(value = "/getPembayaranHariIni", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public DataPembayaran getPembayaranHariIni() {
		DataPembayaran tamp = iMrService.getPembayaranHariIni();
		return tamp;
	}

	@RequestMapping(value = "/getPembayaranByBulan/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataPembayaran> getPembayaranByBulan(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		List<DataPembayaran> tamp = iMrService.getPembayaranByBulan(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getPembayaranByPeriode/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataPembayaran> getPembayaranByPeriode(@PathVariable("awal") String awal,
			@PathVariable("akhir") String akhir) {
		List<DataPembayaran> tamp = iMrService.getPembayaranByPeriode(awal, akhir);
		return tamp;
	}

	/*-----------------------------------------------------Pelaporan Data Kunjungan-----------------------------------------------------------*/

	@RequestMapping(value = "/getDataKunjunganPasienHariIni", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public DataKunjungan getDataKunjunganPasienHariIni() {
		DataKunjungan tamp = iMrService.getDataKunjunganPasienHariIni();
		return tamp;
	}

	@RequestMapping(value = "/getDataKunjunganPasienByBulan/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKunjungan> getDataKunjunganPasienByBulan(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		List<DataKunjungan> tamp = iMrService.getDataKunjunganPasienByBulan(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getDataKunjunganPasienByBulanTersortir/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject getDataKunjunganPasienByBulanTersortir(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		JSONObject tamp = iMrService.getDataKunjunganPasienByBulanTersortir(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getDataKunjunganPasienByPeriode/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKunjungan> getDataKunjunganPasienByPeriode(@PathVariable("awal") String awal,
			@PathVariable("akhir") String akhir) {
		List<DataKunjungan> tamp = iMrService.getDataKunjunganPasienByPeriode(awal, akhir);
		return tamp;
	}

	@RequestMapping(value = "/getDataKunjunganPasienByPeriodeTersortir/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JSONObject getDataKunjunganPasienByPeriodeTersortir(@PathVariable("awal") String awal,
			@PathVariable("akhir") String akhir) {
		JSONObject tamp = iMrService.getDataKunjunganPasienByPeriodeTersortir(awal, akhir);
		return tamp;
	}

	@RequestMapping(value = "/getDataPoliklinikSpesialisDariRegulerByBulan/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKunjungan> getDataPoliklinikSpesialisDariRegulerByBulan(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		List<DataKunjungan> tamp = iMrService.getDataPoliklinikSpesialisDariRegulerByBulan(tahun, bulan);
		return tamp;
	}

	/*-----------------------------------------------------Pelaporan Jumlah Total Kunjungan-----------------------------------------------------------*/

	@GetMapping("getJumlahDaftarKunjunganPasienHariIni")
	public JSONObject getJumlahDaftarKunjunganPasienHariIni() {
		JSONObject tamp = iMrService.getJumlahDaftarKunjunganPasienHariIni();
		return tamp;
	}

	@RequestMapping(value = "/getJumlahDaftarKunjunganPasienByBulan/{tahun}/{bulan}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKunjungan> getJumlahDaftarKunjunganPasienByBulan(@PathVariable("tahun") String tahun,
			@PathVariable("bulan") String bulan) {
		List<DataKunjungan> tamp = iMrService.getJumlahDaftarKunjunganPasienByBulan(tahun, bulan);
		return tamp;
	}

	@RequestMapping(value = "/getJumlahDaftarKunjunganPasienByPeriode/{awal}/{akhir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DataKunjungan> getJumlahDaftarKunjunganPasienByPeriode(@PathVariable("awal") String awal,
			@PathVariable("akhir") String akhir) {
		List<DataKunjungan> tamp = iMrService.getJumlahDaftarKunjunganPasienByPeriode(awal, akhir);
		return tamp;
	}

	/*-----------------------------------------------------Status Kunjungan-----------------------------------------------------------*/

	@GetMapping("getStatusKunjunganByPasienId/{pasienId}")
	public Boolean getStatusKunjunganByPasienId(@PathVariable("pasienId") Integer pasienId) {
		Boolean tamp = iMrService.getStatusKunjunganByPasienId(pasienId);
		return tamp;
	}

	@RequestMapping(value = "/tambahStatusKunjungan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public synchronized ResponseEntity<StatusKunjungan> tambahStatusKunjungan(
			@RequestBody StatusKunjungan statusKunjungan, UriComponentsBuilder builder) {
		iMrService.tambahStatusKunjungan(statusKunjungan);
		return new ResponseEntity<StatusKunjungan>(statusKunjungan, HttpStatus.CREATED);
	}

	/*-----------------------------------------------------Penggabungan noRm-----------------------------------------------------------*/

	@RequestMapping(value = "/penggabunganNoRm", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public synchronized ResponseEntity<WrapperPenggabunganRm> penggabunganNoRm(
			@RequestBody WrapperPenggabunganRm penggabunganRm, UriComponentsBuilder builder) {
		WrapperPenggabunganRm tamp = iMrService.penggabunganNoRm(penggabunganRm);
		return new ResponseEntity<WrapperPenggabunganRm>(tamp, HttpStatus.OK);
	}
}
