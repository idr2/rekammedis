package id.or.pantirapih.mrv01.Service;

public enum StatusCode {
	DATANOTFOUND(100), 
	PAGENOTFOUND(101),
	NULLVALUE(400),
	GETOHERSERVICE(500),
	QUERYERROR(300),
	QUERYSELECT(301),
	QUERYINSERT(302),
	QUERYUPDATE(303),
	QUERYDELETE(304),
	
	JSONOBJ(1),
	JSONARR(2);
	
	private final int code;
	StatusCode(int code) {
		this.code = code;
	}
	public int getCode() {
		return code;
	}
}

