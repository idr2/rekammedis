package id.or.pantirapih.mrv01.Service;

import java.net.MalformedURLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import id.or.pantirapih.mrv01.Model.BpjsDiagnosa;
import id.or.pantirapih.mrv01.Model.BpjsOlahData;
import id.or.pantirapih.mrv01.Model.BpjsTindakan;
import id.or.pantirapih.mrv01.Model.BpjsWrapper;
import id.or.pantirapih.mrv01.Model.DataKasus;
import id.or.pantirapih.mrv01.Model.DataKunjungan;
import id.or.pantirapih.mrv01.Model.DataPasien;
import id.or.pantirapih.mrv01.Model.DataPembayaran;
import id.or.pantirapih.mrv01.Model.Dataolahmr;
import id.or.pantirapih.mrv01.Model.Diagnosa;
import id.or.pantirapih.mrv01.Model.Olahmr;
import id.or.pantirapih.mrv01.Model.StatusKunjungan;
import id.or.pantirapih.mrv01.Model.TanggalLibur;
import id.or.pantirapih.mrv01.Model.Tindakan;
import id.or.pantirapih.mrv01.Model.WrapperPenggabunganRm;

public interface IMrService {

	boolean tambahDataDiagnosa(Diagnosa id) throws MalformedURLException, ParseException;

	boolean tambahDataTindakan(Tindakan id) throws MalformedURLException, ParseException;

	boolean tambahDataMr(Olahmr id) throws MalformedURLException, ParseException;

	void TambahSemuaDataMr(Dataolahmr dataolahmr);

	List<Diagnosa> getDiagnosaMrByIdOlahMr(int id);

	List<Tindakan> getTindakanMrByIdOlahMr(int id);

	Olahmr getDiagnosaMrByNoRegister(String noRegister);

	Olahmr getDiagnosaTindakanMr(String noRegister, Date tanggalPeriksa, String idDokter);
	
	List<Object[]> getOlahMrDanDiagnosaByTanggalHariIni();
	
	List<Object[]> getOlahMrDanDiagnosaBy(Integer bulan, Integer tahun);
	
	List<Object[]> getOlahMrDanDiagnosaByTanggalSatuDanDua(Date tanggalSatu, Date tanggalDua);

	boolean UbahDiagnosa(Diagnosa diagnosa);

	boolean UbahTindakan(Tindakan tindakan);

	boolean UbahDataMr(Olahmr olahmr);
	
	boolean UbahDataMrFinal(Olahmr olahmr);

	void UbahSemuaDataMr(Dataolahmr dataolahmr);
	
	void UbahSemuaDataMrFinal(Dataolahmr dataolahmr);

	boolean hapusDiagnosa(Diagnosa diagnosa);

	boolean HapusTindakan(Tindakan tindakan);

	boolean tambahDataMrFinal(Olahmr id)throws MalformedURLException, ParseException;

	void TambahSemuaDataMrFinal(Dataolahmr dataolahmr);

	
	JSONObject getBpjsOlahDataByNoRegister(String noRegister);
	void tambahBpjsOlahData(BpjsWrapper bpjsWrapper);
	void ubahBpjsOlahData(BpjsOlahData bpjsOlahData);
	void ubahBpjsDiagnosa(BpjsDiagnosa bpjsDiagnosa);
	void ubahBpjsTindakan(BpjsTindakan bpjsTindakan);

	DataPasien getDataPasienHariIni();
	List<DataPasien> getDataPasienByBulan(String tahun, String bulan);
	JSONObject getDataPasienByBulanTersortir(String tahun, String bulan);
	JSONObject getDataPasienByPeriodeTersortir(String awal, String akhir);
	
	
	List<TanggalLibur> getTanggalLibur(String tahun, String bulan);
	List<TanggalLibur> getTanggalLiburByPeriode(String awal, String akhir);
	void tambahTanggalLibur(TanggalLibur tanggalLibur);

	DataKasus getKasusHariIni();
	List<DataKasus> getKasusByBulan(String tahun, String bulan);
	List<DataKasus> getKasusByPeriode(String awal, String akhir);

	DataPembayaran getPembayaranHariIni();
	List<DataPembayaran> getPembayaranByBulan(String tahun, String bulan);
	List<DataPembayaran> getPembayaranByPeriode(String awal, String akhir);

	DataKunjungan getDataKunjunganPasienHariIni();
	List<DataKunjungan> getDataKunjunganPasienByBulan(String tahun, String bulan);
	JSONObject getDataKunjunganPasienByBulanTersortir(String tahun, String bulan);
	List<DataKunjungan> getDataKunjunganPasienByPeriode(String awal, String akhir);
	JSONObject getDataKunjunganPasienByPeriodeTersortir(String awal, String akhir);
	JSONObject getJumlahDaftarKunjunganPasienHariIni();
	List<DataKunjungan> getJumlahDaftarKunjunganPasienByBulan(String tahun, String bulan);
	List<DataKunjungan> getJumlahDaftarKunjunganPasienByPeriode(String awal, String akhir);
	List<DataKunjungan> getDataPoliklinikSpesialisDariRegulerByBulan(String tahun, String bulan);

	Boolean getStatusKunjunganByPasienId(Integer pasienId);
	void tambahStatusKunjungan(StatusKunjungan statusKunjungan);

	WrapperPenggabunganRm penggabunganNoRm(WrapperPenggabunganRm penggabunganRm);

	boolean cekTanggalLibur(String tanggal);

	

}
