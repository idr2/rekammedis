package id.or.pantirapih.mrv01.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import id.or.pantirapih.mrv01.DAO.IMrDAO;
import id.or.pantirapih.mrv01.Exception.MrException;
import id.or.pantirapih.mrv01.Model.BpjsDiagnosa;
import id.or.pantirapih.mrv01.Model.BpjsOlahData;
import id.or.pantirapih.mrv01.Model.BpjsTindakan;
import id.or.pantirapih.mrv01.Model.BpjsWrapper;
import id.or.pantirapih.mrv01.Model.DataKasus;
import id.or.pantirapih.mrv01.Model.DataKunjungan;
import id.or.pantirapih.mrv01.Model.DataPasien;
import id.or.pantirapih.mrv01.Model.DataPembayaran;
import id.or.pantirapih.mrv01.Model.Dataolahmr;
import id.or.pantirapih.mrv01.Model.Diagnosa;
import id.or.pantirapih.mrv01.Model.Olahmr;
import id.or.pantirapih.mrv01.Model.StatusKunjungan;
import id.or.pantirapih.mrv01.Model.TanggalLibur;
import id.or.pantirapih.mrv01.Model.Tindakan;
import id.or.pantirapih.mrv01.Model.WrapperPenggabunganRm;

@Service
public class MrService implements IMrService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private IMrDAO iMrDAO;

	// url develop
//	String pasienV3URL = "http://192.168.11.183:8080/PasienV2/PasienV2/";
//	String olahMrURL = "http://192.168.11.212:8080/Mr-v01/RekamMedis/";
//	String pendaftaranV3URL = "http://192.168.11.211:8080/PendaftaranV3/PendaftaranV3/";
//	String poliLukasURL = "http://192.168.11.184:8080/PoliLukas/PoliLukas/";
//	String jadwalURL = "http://192.168.11.213:8080/jadwaldokter-v04-0.0.1/Jadwal/";
//	String elektroURL = "http://192.168.11.184:8080/Elektromedis/Elektromedis/";
//	String hemodURL = "http://192.168.11.184:8080/Hemodialisa/Hemodialisa/";
//	String fisioURL = "192.168.11.184:8080/Fisioterapi/Fisioterapi/";
//	String radiolURL = "http://192.168.11.216:8080/Radiologi/Radiologi/";
//	String labURL = "http://192.168.11.106:8080/laboratorium/lab/";
//	String bankURL = "http://192.168.11.182:8080/Bank/Bank/";
//	String farmasiURL = "http://192.168.11.214:8080/Farmasi/Farmasi/";

	// url deploy
//	String pasienV3URL = "http://192.168.11.183:8080/PasienVer4/Pasien/";
//	String olahMrURL = "http://192.168.11.212:8080/MrVer4/RekamMedis/";
//	String pendaftaranV3URL = "http://192.168.11.211:8080/PendaftaranVer4/Pendaftaran/";
////	String poliLukasURL = "http://192.168.11.184:8080/PoliLukasVer4/PoliLukas/";
//	String poliLukasURL = "http://192.168.27.15:8888/PoliLukas/";
//	String jadwalURL = "http://192.168.11.213:8080/jadwaldokterVer4/Jadwal/";
//	String elektroURL = "http://192.168.11.184:8080/ElektromedisVer4/Elektromedis/";
//	String hemodURL = "http://192.168.11.184:8080/HemodialisaVer4/Hemodialisa/";
//	String fisioURL = "192.168.11.184:8080/FisioterapiVer4/Fisioterapi/";
//	String radiolURL = "http://192.168.11.216:8080/RadiologiVer4/Radiologi/";
//	String labURL = "http://192.168.11.106:8080/laboratoriumVer4/lab/";
//	String bankURL = "http://192.168.11.182:8080/BankVer4/Bank/";
//	String farmasiURL = "http://192.168.11.214:8080/FarmasiVer4/Farmasi/";
	
	// url Kuber
	String pasienV3URL = "http://kubernetes.pantirapih.id/Pasien/";
	String olahMrURL = "http://kubernetes.pantirapih.id/Mr/RekamMedis/";
	String pendaftaranV3URL = "http://kubernetes.pantirapih.id/Pendaftaran/";
//	String poliLukasURL = "http://192.168.11.184:8080/PoliLukasVer4/PoliLukas/";
	String poliLukasURL = "http://kubernetes.pantirapih.id/PoliLukas/";
	String jadwalURL = "http://kubernetes.pantirapih.id/Jadwal/";
	String elektroURL = "http://kubernetes.pantirapih.id/Elektromedis/";
	String hemodURL = "http://kubernetes.pantirapih.id/Hemodialisa/";
	String fisioURL = "http://kubernetes.pantirapih.id/Fisioterapi/";
	String radiolURL = "http://kubernetes.pantirapih.id/Radiologi/";
	String labURL = "http://kubernetes.pantirapih.id/lab/";
	String bankURL = "http://kubernetes.pantirapih.id/Bank/";
	String farmasiURL = "http://kubernetes.pantirapih.id/Farmasi/";

	public org.json.simple.JSONObject parserJsonObj(String data)
			throws MalformedURLException, org.json.simple.parser.ParseException {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONObject json = (org.json.simple.JSONObject) parser.parse(data);

		return json;
	}

	public JSONArray parserJsonArr(String data) throws MalformedURLException, org.json.simple.parser.ParseException {
		JSONParser parser = new JSONParser();
		JSONArray json = (JSONArray) parser.parse(data);

		return json;
	}

	public JSONArray getOtherServiceArray(URL uri) throws ParseException, MalformedURLException {
		String result = null;
		JSONObject tamp = null;
		JSONArray tampArr = null;
		// System.out.println("Ini url nya = " + uri.toString());
		try {
			RestTemplate restTemplate = new RestTemplate();
			result = restTemplate.getForObject(uri.toString(), String.class);
			tampArr = parserJsonArr(result);
			if (tampArr.size() != 0) {
				tamp = (JSONObject) tampArr.get(0);
				if (tamp.containsKey("errorCode")) {
					if (tamp.containsKey("errorCause")) {
						if (tamp.containsKey("errorMessage"))
							throw new MrException(tamp.get("errorCause").toString(),
									tamp.get("errorMessage").toString(),
									Integer.valueOf(tamp.get("errorCode").toString()));
						else
							throw new MrException(tamp.get("errorCause").toString(), "Error",
									Integer.valueOf(tamp.get("errorCode").toString()));
					} else
						throw new MrException("Error", tamp.get("errorMessage").toString(),
								Integer.valueOf(tamp.get("errorCode").toString()));
				}
			}
		} catch (MrException e) {
			throw new MrException(e.getMessage() + ", " + e.getErrorMessage() + ", " + e.getErrorCode(), "",
					StatusCode.GETOHERSERVICE.getCode());
		} catch (Exception e) {
			throw new MrException(e.getCause() + ", " + uri.toString(), "", StatusCode.GETOHERSERVICE.getCode());
		}
		return tampArr;
	}

	public org.json.simple.JSONObject getOtherService(URL uri) throws ParseException, MalformedURLException {
		String result = null;
		JSONObject tamp = null;
		// System.out.println("url = " + uri.toString());
		try {
			RestTemplate restTemplate = new RestTemplate();
			result = restTemplate.getForObject(uri.toString(), String.class);
			tamp = parserJsonObj(result);
			if (tamp.containsKey("errorCode")) {
				if (tamp.containsKey("errorCause")) {
					if (tamp.containsKey("errorMessage"))
						throw new MrException(tamp.get("errorCause").toString(), tamp.get("errorMessage").toString(),
								Integer.valueOf(tamp.get("errorCode").toString()));
					else
						throw new MrException(tamp.get("errorCause").toString(), "Error",
								Integer.valueOf(tamp.get("errorCode").toString()));
				} else
					throw new MrException("Error", tamp.get("errorMessage").toString(),
							Integer.valueOf(tamp.get("errorCode").toString()));
			}
		} catch (MrException e) {
			throw new MrException(e.getMessage() + ", " + e.getErrorMessage() + ", " + e.getErrorCode(), "",
					StatusCode.GETOHERSERVICE.getCode());
		} catch (Exception e) {
			throw new MrException(e.getCause().getMessage() + ", " + uri.toString(), "",
					StatusCode.GETOHERSERVICE.getCode());
		}
		return tamp;
	}

	@Override
	public boolean tambahDataDiagnosa(Diagnosa id) throws MalformedURLException, ParseException {
		iMrDAO.tambahDataDiagnosa(id);
		return true;
	}

	@Override
	public boolean tambahDataTindakan(Tindakan id) throws MalformedURLException, ParseException {
		iMrDAO.tambahDataTindakan(id);
		return true;
	}

	@Override
	public boolean tambahDataMr(Olahmr id) throws MalformedURLException, ParseException {
		iMrDAO.tambahDataMr(id);
		return true;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void TambahSemuaDataMr(Dataolahmr dataolahmr) {
		try {
			boolean flagKlaim = false;
			synchronized (this) {
				Olahmr id = dataolahmr.getOlahmr();
				flagKlaim = tambahDataMr(id);

				for (Diagnosa diagnosa : dataolahmr.getDiagnosa()) {
					diagnosa.setIdOlahMr(id.getIdOlahMr());
					tambahDataDiagnosa(diagnosa);
				}
				for (Tindakan tindakan : dataolahmr.getTindakan()) {
					tindakan.setIdOlahMr(id.getIdOlahMr());
					tambahDataTindakan(tindakan);
				}

			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Menyimpan Data MR", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public List<Diagnosa> getDiagnosaMrByIdOlahMr(int id) {
		List<Diagnosa> tamp = null;
		try {
			tamp = iMrDAO.getDiagnosaMrByIdOlahMr(id);
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		return tamp;
	}

	@Override
	public List<Tindakan> getTindakanMrByIdOlahMr(int id) {
		List<Tindakan> tamp = null;
		try {
			tamp = iMrDAO.getTindakanMrByIdOlahMr(id);
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		return tamp;
	}

	@Override
	public Olahmr getDiagnosaMrByNoRegister(String noRegister) {
		return iMrDAO.getDiagnosaMrByNoRegister(noRegister);
	}

	@Override
	public Olahmr getDiagnosaTindakanMr(String noRegister, Date tanggalPeriksa, String idDokter) {
		return iMrDAO.getDiagnosaTindakanMr(noRegister, tanggalPeriksa, idDokter);
	}

	@Override
	public boolean UbahDiagnosa(Diagnosa diagnosa) {
		iMrDAO.UbahDiagnosa(diagnosa);
		return true;
	}

	@Override
	public boolean UbahTindakan(Tindakan tindakan) {
		iMrDAO.UbahTindakan(tindakan);
		return true;
	}

	@Override
	public boolean UbahDataMr(Olahmr olahmr) {
		iMrDAO.UbahDataMr(olahmr);
		return true;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void UbahSemuaDataMr(Dataolahmr dataolahmr) {
		try {
			boolean flagKlaim = false;
			synchronized (this) {
				Olahmr id = dataolahmr.getOlahmr();
				int ids = id.getIdOlahMr();
				flagKlaim = UbahDataMr(id);

				for (Diagnosa diagnosa : dataolahmr.getDiagnosa()) {
					if (diagnosa.getIdDiagnosaOlahMr() != null) {
						if (diagnosa.getKeterangan() != null) {
							UbahDiagnosa(diagnosa);
						} else {
							hapusDiagnosa(diagnosa);
						}
					} else {
						diagnosa.setIdOlahMr(id.getIdOlahMr());
						tambahDataDiagnosa(diagnosa);
					}

				}
				for (Tindakan tindakan : dataolahmr.getTindakan()) {

					if (tindakan.getIdTindakan() != null) {
						if (tindakan.getNamaTindakan() != null) {
							UbahTindakan(tindakan);
						} else {
							HapusTindakan(tindakan);
						}
					} else {
						tindakan.setIdOlahMr(id.getIdOlahMr());
						tambahDataTindakan(tindakan);
					}

				}

			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Menyimpan Data MR", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public boolean hapusDiagnosa(Diagnosa diagnosa) {
		iMrDAO.hapusDiagnosa(diagnosa);
		return true;
	}

	@Override
	public boolean HapusTindakan(Tindakan tindakan) {
		iMrDAO.HapusTindakan(tindakan);
		return true;
	}

	@Override
	public boolean tambahDataMrFinal(Olahmr id) throws MalformedURLException, ParseException {
		iMrDAO.tambahDataMrFinal(id);
		return true;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void TambahSemuaDataMrFinal(Dataolahmr dataolahmr) {
		try {
			boolean flagKlaim = false;
			synchronized (this) {
				Olahmr id = dataolahmr.getOlahmr();
				flagKlaim = tambahDataMrFinal(id);

				for (Diagnosa diagnosa : dataolahmr.getDiagnosa()) {
					diagnosa.setIdOlahMr(id.getIdOlahMr());
					tambahDataDiagnosa(diagnosa);
				}
				for (Tindakan tindakan : dataolahmr.getTindakan()) {
					tindakan.setIdOlahMr(id.getIdOlahMr());
					tambahDataTindakan(tindakan);
				}
			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Menyimpan Data MR", StatusCode.QUERYINSERT.getCode());
		}
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void UbahSemuaDataMrFinal(Dataolahmr dataolahmr) {
		try {
			boolean flagKlaim = false;
			synchronized (this) {
				Olahmr id = dataolahmr.getOlahmr();
				int ids = id.getIdOlahMr();
				flagKlaim = UbahDataMrFinal(id);

				for (Diagnosa diagnosa : dataolahmr.getDiagnosa()) {
					if (diagnosa.getIdDiagnosaOlahMr() != null) {
						if (diagnosa.getKeterangan() != null) {
							UbahDiagnosa(diagnosa);
						} else {
							hapusDiagnosa(diagnosa);
						}
					} else {
						diagnosa.setIdOlahMr(id.getIdOlahMr());
						tambahDataDiagnosa(diagnosa);
					}

				}
				for (Tindakan tindakan : dataolahmr.getTindakan()) {

					if (tindakan.getIdTindakan() != null) {
						if (tindakan.getNamaTindakan() != null) {
							UbahTindakan(tindakan);
						} else {
							HapusTindakan(tindakan);
						}
					} else {
						tindakan.setIdOlahMr(id.getIdOlahMr());
						tambahDataTindakan(tindakan);
					}

				}

			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Menyimpan Data MR", StatusCode.QUERYINSERT.getCode());
		}

	}

	@Override
	public boolean UbahDataMrFinal(Olahmr olahmr) {
		iMrDAO.UbahDataMrFinal(olahmr);
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getBpjsOlahDataByNoRegister(String noRegister) {
		JSONObject tamp = new JSONObject();
		ArrayList<String> tampError = new ArrayList<String>();
		BpjsOlahData bpjsOlahData = null;
		JSONObject tampOlahMr = new JSONObject();
		try {
			try {
				bpjsOlahData = iMrDAO.getBpjsOlahDataByNoRegister(noRegister);
			} catch (Exception e) {
				tampError.add("Tidak Ada BPJS Olah Data");
			}
			try {
				Olahmr olahMr = iMrDAO.getDiagnosaMrByNoRegister(noRegister);
				tampOlahMr = new JSONObject();
				List<Diagnosa> diagOlahMr = getDiagnosaMrByIdOlahMr(olahMr.getIdOlahMr());
				List<Tindakan> tindakanOlahMr = getTindakanMrByIdOlahMr(olahMr.getIdOlahMr());
				tampOlahMr.put("olahMr", olahMr);
				tampOlahMr.put("diagOlahMr", diagOlahMr);
				tampOlahMr.put("tindakanOlahMr", tindakanOlahMr);

			} catch (Exception e) {
				tampError.add("Tidak Ada Olah MR");
			}
			if (bpjsOlahData != null && tampOlahMr != null) {
				tamp.put("bpjsOlahData", bpjsOlahData);
			} else if (bpjsOlahData == null && tampOlahMr != null) {
				tamp.put("olahMr", tampOlahMr);
			} else if (bpjsOlahData != null && tampOlahMr == null) {
				tamp.put("bpjsOlahData", bpjsOlahData);
			} else {
				throw new MrException("", tampError, StatusCode.QUERYSELECT.getCode());
			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), tampError, StatusCode.QUERYSELECT.getCode());
		}

		return tamp;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void tambahBpjsOlahData(BpjsWrapper bpjsWrapper) {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			bpjsWrapper.getBpjsOlahData().setAddDate(timeStamp);
			bpjsWrapper.getBpjsOlahData().setUpdateDate(timeStamp);
			bpjsWrapper.getBpjsOlahData().setDeleted(false);
			iMrDAO.tambahBpjsOlahData(bpjsWrapper.getBpjsOlahData());
			Long idBpjsOlahData = bpjsWrapper.getBpjsOlahData().getIdBpjsOlahData();
			Integer parseIdBpjsOlah = idBpjsOlahData.intValue();

			for (BpjsDiagnosa diag : bpjsWrapper.getBpjsDiagnosa()) {
				diag.setBpjsOlahId(parseIdBpjsOlah);
				iMrDAO.tambahBpjsDiagnosa(diag);
			}
			for (BpjsTindakan tind : bpjsWrapper.getBpjsTindakan()) {
				tind.setBpjsOlahId(parseIdBpjsOlah);
				iMrDAO.tambahBpjsTindakan(tind);
			}

		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Menyimpan BPJS Olah Data", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public void ubahBpjsOlahData(BpjsOlahData bpjsOlahData) {
		try {
			iMrDAO.ubahBpjsOlahData(bpjsOlahData);
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Ubah BPJS Olah Data", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public void ubahBpjsDiagnosa(BpjsDiagnosa bpjsDiagnosa) {
		try {
			iMrDAO.ubahBpjsDiagnosa(bpjsDiagnosa);
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Ubah BPJS Diagnosa", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public void ubahBpjsTindakan(BpjsTindakan bpjsTindakan) {
		try {
			iMrDAO.ubahBpjsTindakan(bpjsTindakan);
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Ubah BPJS Tindakan", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public DataPasien getDataPasienHariIni() {
		DataPasien dataPas = new DataPasien();
		try {
			String jam = "";
			ArrayList<String> tampJamPagiLama = new ArrayList<String>();
			ArrayList<String> tampJamSiangLama = new ArrayList<String>();
			ArrayList<String> tampJamMalamLama = new ArrayList<String>();
			ArrayList<String> tampJamPagiBaru = new ArrayList<String>();
			ArrayList<String> tampJamSiangBaru = new ArrayList<String>();
			ArrayList<String> tampJamMalamBaru = new ArrayList<String>();

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);

			URL urlPasienLamaByById = new URL(pasienV3URL + "getPasienLamaHariIni");
			org.json.simple.JSONArray pasienLama = getOtherServiceArray(urlPasienLamaByById);
			for (int j = 0; j < pasienLama.size(); j++) {
				JSONObject pasien = (JSONObject) pasienLama.get(j);
				jam = pasien.get("jamDaftar").toString();
				java.util.Date jamAsli = sdf.parse(jam);

				if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
					tampJamPagiLama.add(jam);
				} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
					tampJamSiangLama.add(jam);
				} else
					tampJamMalamLama.add(jam);
			}

			URL urlPasienBaruByById = new URL(pasienV3URL + "getPasienBaruHariIni");
			org.json.simple.JSONArray pasienBaru = getOtherServiceArray(urlPasienBaruByById);
			for (int j = 0; j < pasienBaru.size(); j++) {
				JSONObject pasien = (JSONObject) pasienBaru.get(j);
				jam = pasien.get("jamDaftar").toString();
				java.util.Date jamAsli = sdf.parse(jam);

				if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
					tampJamPagiBaru.add(jam);
				} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
					tampJamSiangBaru.add(jam);
				} else
					tampJamMalamBaru.add(jam);
			}

			dataPas.setPagiBaru(tampJamPagiBaru.size());
			dataPas.setPagiLama(tampJamPagiLama.size());
			dataPas.setPagiJumlah(tampJamPagiBaru.size() + tampJamPagiLama.size());
			dataPas.setSiangBaru(tampJamSiangBaru.size());
			dataPas.setSiangLama(tampJamSiangLama.size());
			dataPas.setSiangJumlah(tampJamSiangBaru.size() + tampJamSiangLama.size());
			dataPas.setMalamBaru(tampJamMalamBaru.size());
			dataPas.setMalamLama(tampJamMalamLama.size());
			dataPas.setMalamJumlah(tampJamMalamBaru.size() + tampJamMalamLama.size());
			dataPas.setTotalBaru(dataPas.getPagiBaru() + dataPas.getSiangBaru() + dataPas.getMalamBaru());
			dataPas.setTotalLama(dataPas.getPagiLama() + dataPas.getSiangLama() + dataPas.getMalamLama());
			dataPas.setTotalSemua(dataPas.getTotalBaru() + dataPas.getTotalLama());
			dataPas.setTanggal(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYSELECT.getCode());
		}
		return dataPas;
	}

	@Override
	public List<TanggalLibur> getTanggalLibur(String tahun, String bulan) {
		List<TanggalLibur> tamp = null;
		try {
			tamp = iMrDAO.getTanggalLibur(tahun, bulan);
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		return tamp;
	}

	@Override
	public List<TanggalLibur> getTanggalLiburByPeriode(String awal, String akhir) {
		List<TanggalLibur> tamp = null;
		try {
			tamp = iMrDAO.getTanggalLiburByPeriode(awal, akhir);
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.QUERYINSERT.getCode());
		}
		return tamp;
	}
	
	@Override
	public boolean cekTanggalLibur(String tanggal) {
		boolean statusLibur = false;
		try {
			statusLibur = iMrDAO.cekTanggalLibur(tanggal);
		} catch (Exception e) {
			return statusLibur;
		}
		return statusLibur;
	}
	
	@Override
	public void tambahTanggalLibur(TanggalLibur tanggalLibur) {
		try {
			iMrDAO.tambahTanggalLibur(tanggalLibur);
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "Gagal Tambah Tanggal Libur", StatusCode.QUERYINSERT.getCode());
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject getDataPasienByBulanTersortir(String tahun, String bulan) {
		JSONObject tamp = new JSONObject();
		JSONArray libur = new JSONArray();
		JSONArray kerja = new JSONArray();

		List<DataPasien> pasien = getDataPasienByBulan(tahun, bulan);
		List<TanggalLibur> tanggalLbr = getTanggalLibur(tahun, bulan);

		if (pasien.size() < tanggalLbr.size()) {
			throw new MrException("Tanggal Libur lebih banyak dari Tanggal Kunjungan",
					StatusCode.DATANOTFOUND.getCode());
		}

		String tglPasien = "";
		String tglLbr = "";

		// tgl libur 3, 6
		// data kunjungan 3, 6, 7
		// data kunjungan harus > tgl libur

		for (DataPasien pas : pasien) {
			boolean found = false;
			DataPasien tampz = new DataPasien();

			for (TanggalLibur lbr : tanggalLbr) {
				tampz = pas;
				tglPasien = pas.getTanggal();
				tglLbr = lbr.getTanggal();

				if (tglPasien.equalsIgnoreCase(tglLbr)) {
					libur.add(tampz);
					found = true;
					break;
				} else {
					found = false;
				}
			}

			if (found == false) {
				kerja.add(tampz);
				System.out.println("kerja");
			}

		}
		tamp.put("libur", libur);
		tamp.put("kerja", kerja);

		return tamp;
	}

	@Override
	public List<DataPasien> getDataPasienByBulan(String tahun, String bulan) {
		List<DataPasien> tamp = new ArrayList<>();
		String tanggal = "";
		String jam = "";
		String statusPasien = "";
		try {
			List<List<String>> array = new ArrayList<List<String>>();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);
			String ambilTanggal = "";
			String tampStatus = "";

			URL urlPasienByById = new URL(pasienV3URL + "getPasienByBulan/" + tahun + "/" + bulan);
			org.json.simple.JSONArray pasienBulk = getOtherServiceArray(urlPasienByById);
			for (int a = 0; a < pasienBulk.size(); a++) {
				JSONObject pasien = (JSONObject) pasienBulk.get(a);
				jam = pasien.get("jamDaftar").toString();
				tanggal = pasien.get("tanggalDaftar").toString();
				statusPasien = pasien.get("statusPasien").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(tanggal)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			ambilTanggal = "";
			String tanggalLama = "";
			for (int i = 0; i < array.size(); i++) {
				List<String> potongArray = array.get(i);
				DataPasien dataKunj = new DataPasien();
				Integer tampJamPagiBaru = 0;
				Integer tampJamSiangBaru = 0;
				Integer tampJamMalamBaru = 0;
				Integer tampJamPagiLama = 0;
				Integer tampJamSiangLama = 0;
				Integer tampJamMalamLama = 0;
				ambilTanggal = potongArray.get(0);

				if (tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					dataKunj = tamp.get(tamp.size() - 1);
					tampJamPagiBaru = tamp.get(tamp.size() - 1).getPagiBaru();
					tampJamSiangBaru = tamp.get(tamp.size() - 1).getSiangBaru();
					tampJamMalamBaru = tamp.get(tamp.size() - 1).getMalamBaru();
					tampJamPagiLama = tamp.get(tamp.size() - 1).getPagiLama();
					tampJamSiangLama = tamp.get(tamp.size() - 1).getSiangLama();
					tampJamMalamLama = tamp.get(tamp.size() - 1).getMalamLama();
				}

				for (int j = 0; j < potongArray.size(); j++) {
					dataKunj.setTanggal(ambilTanggal);

					tampStatus = potongArray.get(1);
					if (tampStatus.equalsIgnoreCase("Lama")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiLama++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangLama++;
							} else {
								tampJamMalamLama++;
							}
						}

					} else if (tampStatus.equalsIgnoreCase("Baru")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiBaru++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangBaru++;
							} else {
								tampJamMalamBaru++;
							}
						}
					}
				}

				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(dataKunj.getMalamBaru() + dataKunj.getMalamLama());
				dataKunj.setPagiJumlah(dataKunj.getPagiBaru() + dataKunj.getPagiLama());
				dataKunj.setSiangJumlah(dataKunj.getSiangBaru() + dataKunj.getSiangLama());
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());

				if (!tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					tanggalLama = ambilTanggal;
					tamp.add(dataKunj);
				}
			}

		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYSELECT.getCode());
		}
		return tamp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getDataPasienByPeriodeTersortir(String awal, String akhir) {
		JSONObject tamp = new JSONObject();
		JSONArray libur = new JSONArray();
		JSONArray kerja = new JSONArray();

		List<DataPasien> kunjunganPasien = getDataPasienByPeriode(awal, akhir);
		List<TanggalLibur> tanggalLbr = getTanggalLiburByPeriode(awal, akhir);

		if (kunjunganPasien.size() < tanggalLbr.size()) {
			throw new MrException("Tanggal Libur lebih banyak dari Tanggal Kunjungan",
					StatusCode.DATANOTFOUND.getCode());
		}

		String tglPasien = "";
		String tglLbr = "";

		for (DataPasien pas : kunjunganPasien) {
			boolean found = false;
			DataPasien tampz = new DataPasien();

			for (TanggalLibur lbr : tanggalLbr) {
				tampz = pas;
				tglPasien = pas.getTanggal();
				tglLbr = lbr.getTanggal();

				if (tglPasien.equalsIgnoreCase(tglLbr)) {
					libur.add(tampz);
					found = true;
					break;
				} else {
					found = false;
				}
			}

			if (found == false) {
				kerja.add(tampz);
				System.out.println("kerja");
			}

		}
		tamp.put("libur", libur);
		tamp.put("kerja", kerja);

		return tamp;
	}

	private List<DataPasien> getDataPasienByPeriode(String awal, String akhir) {
		List<DataPasien> tamp = new ArrayList<>();
		String tanggal = "";
		String jam = "";
		String statusPasien = "";
		try {
			List<List<String>> array = new ArrayList<List<String>>();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);
			String ambilTanggal = "";
			String tampStatus = "";

			URL urlPasienByById = new URL(pasienV3URL + "getPasienByPeriode/" + awal + "/" + akhir);
			org.json.simple.JSONArray pasienBulk = getOtherServiceArray(urlPasienByById);
			for (int a = 0; a < pasienBulk.size(); a++) {
				JSONObject pasien = (JSONObject) pasienBulk.get(a);
				jam = pasien.get("jamDaftar").toString();
				tanggal = pasien.get("tanggalDaftar").toString();
				statusPasien = pasien.get("statusPasien").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(tanggal)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			ambilTanggal = "";
			String tanggalLama = "";
			for (int i = 0; i < array.size(); i++) {
				List<String> potongArray = array.get(i);
				DataPasien dataKunj = new DataPasien();
				Integer tampJamPagiBaru = 0;
				Integer tampJamSiangBaru = 0;
				Integer tampJamMalamBaru = 0;
				Integer tampJamPagiLama = 0;
				Integer tampJamSiangLama = 0;
				Integer tampJamMalamLama = 0;
				ambilTanggal = potongArray.get(0);

				if (tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					dataKunj = tamp.get(tamp.size() - 1);
					tampJamPagiBaru = tamp.get(tamp.size() - 1).getPagiBaru();
					tampJamSiangBaru = tamp.get(tamp.size() - 1).getSiangBaru();
					tampJamMalamBaru = tamp.get(tamp.size() - 1).getMalamBaru();
					tampJamPagiLama = tamp.get(tamp.size() - 1).getPagiLama();
					tampJamSiangLama = tamp.get(tamp.size() - 1).getSiangLama();
					tampJamMalamLama = tamp.get(tamp.size() - 1).getMalamLama();
				}

				for (int j = 0; j < potongArray.size(); j++) {
					dataKunj.setTanggal(ambilTanggal);

					tampStatus = potongArray.get(1);
					if (tampStatus.equalsIgnoreCase("Lama")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiLama++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangLama++;
							} else {
								tampJamMalamLama++;
							}
						}

					} else if (tampStatus.equalsIgnoreCase("Baru")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiBaru++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangBaru++;
							} else {
								tampJamMalamBaru++;
							}
						}
					}
				}

				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(dataKunj.getMalamBaru() + dataKunj.getMalamLama());
				dataKunj.setPagiJumlah(dataKunj.getPagiBaru() + dataKunj.getPagiLama());
				dataKunj.setSiangJumlah(dataKunj.getSiangBaru() + dataKunj.getSiangLama());
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());

				if (!tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					tanggalLama = ambilTanggal;
					tamp.add(dataKunj);
				}
			}

		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYSELECT.getCode());
		}
		return tamp;
	}

	@Override
	public List<Object[]> getOlahMrDanDiagnosaByTanggalHariIni() {
		List<Object[]> tamp = null;
		try {
			tamp = iMrDAO.getOlahMrDanDiagnosaByTanggalHariIni();
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return tamp;
	}

	@Override
	public List<Object[]> getOlahMrDanDiagnosaBy(Integer bulan, Integer tahun) {
		List<Object[]> tamp = null;
		try {
			tamp = iMrDAO.getOlahMrDanDiagnosaBy(bulan, tahun);
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return tamp;
	}

	@Override
	public List<Object[]> getOlahMrDanDiagnosaByTanggalSatuDanDua(Date tanggalSatu, Date tanggalDua) {
		List<Object[]> tamp = null;
		try {
			tamp = iMrDAO.getOlahMrDanDiagnosaByTanggalSatuDanDua(tanggalSatu, tanggalDua);
		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		if (tamp.size() == 0) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return tamp;
	}

	@Override
	public DataKasus getKasusHariIni() {
		DataKasus dataKas = new DataKasus();
		try {
			URL urlPasienByById = new URL(olahMrURL + "getOlahMrDanDiagnosaByTanggalHariIni");
			JSONArray olahMR = getOtherServiceArray(urlPasienByById);
			for (Object obj : olahMR) {
				Integer kasusLama = 0;
				Integer kasusBaru = 0;

				JSONObject oba = (JSONObject) obj;
				JSONArray diag = (JSONArray) oba.get("Diagnosa");
				for (int i = 0; i < diag.size(); i++) {
					JSONObject aa = (JSONObject) diag.get(i);
					String status = aa.get("status").toString();
					if (status.equalsIgnoreCase("L")) {
						kasusLama++;
					} else {
						kasusBaru++;
					}
				}
				dataKas.setTanggal(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
				dataKas.setKasusBaru(kasusBaru);
				dataKas.setKasusLama(kasusLama);
				dataKas.setJumlahKasus(kasusBaru + kasusLama);
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}

		return dataKas;
	}

	@Override
	public List<DataKasus> getKasusByBulan(String tahun, String bulan) {
		List<DataKasus> dataKasList = new ArrayList<>();
		try {
			URL urlPasienByById = new URL(olahMrURL + "getOlahMrDanDiagnosaBy/" + bulan + "/" + tahun);
			JSONArray olahMR = getOtherServiceArray(urlPasienByById);
			String ambilTanggal = "";
			String tanggalLama = "";
			for (Object obj : olahMR) {
				DataKasus dataKas = new DataKasus();
				Integer kasusLama = 0;
				Integer kasusBaru = 0;

				JSONObject oba = (JSONObject) obj;
				JSONArray diag = (JSONArray) oba.get("Diagnosa");
				ambilTanggal = oba.get("tanggalPeriksa").toString();

				for (int i = 0; i < diag.size(); i++) {

					JSONObject aa = (JSONObject) diag.get(i);
					String status = aa.get("status").toString();
					if (status.equalsIgnoreCase("L")) {
						kasusLama++;
					} else {
						kasusBaru++;
					}
				}

				if (!tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					tanggalLama = ambilTanggal;
					dataKas.setTanggal(ambilTanggal);
					dataKas.setKasusBaru(kasusBaru);
					dataKas.setKasusLama(kasusLama);
					dataKas.setJumlahKasus(kasusBaru + kasusLama);
				}
				dataKasList.add(dataKas);
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return dataKasList;
	}

	@Override
	public List<DataKasus> getKasusByPeriode(String awal, String akhir) {
		List<DataKasus> dataKasList = new ArrayList<>();
		try {
			URL urlPasienByById = new URL(olahMrURL + "getOlahMrDanDiagnosaByTanggalSatuDanDua/" + awal + "/" + akhir);
			JSONArray olahMR = getOtherServiceArray(urlPasienByById);
			String ambilTanggal = "";
			String tanggalLama = "";
			for (Object obj : olahMR) {
				DataKasus dataKas = new DataKasus();
				Integer kasusLama = 0;
				Integer kasusBaru = 0;

				JSONObject oba = (JSONObject) obj;
				JSONArray diag = (JSONArray) oba.get("Diagnosa");
				ambilTanggal = oba.get("tanggalPeriksa").toString();

				for (int i = 0; i < diag.size(); i++) {

					JSONObject aa = (JSONObject) diag.get(i);
					String status = aa.get("status").toString();
					if (status.equalsIgnoreCase("L")) {
						kasusLama++;
					} else {
						kasusBaru++;
					}
				}

				if (!tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					tanggalLama = ambilTanggal;
					dataKas.setTanggal(ambilTanggal);
					dataKas.setKasusBaru(kasusBaru);
					dataKas.setKasusLama(kasusLama);
					dataKas.setJumlahKasus(kasusBaru + kasusLama);
				}
				dataKasList.add(dataKas);
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return dataKasList;
	}

	@Override
	public DataPembayaran getPembayaranHariIni() {
		DataPembayaran dataKunj = new DataPembayaran();
		try {
			URL urlPendaftaran = new URL(pendaftaranV3URL + "getKunjunganPasienDistinctHariIni");
			JSONArray pendaftaranBulk = getOtherServiceArray(urlPendaftaran);

			Integer bpjs = 0;
			Integer non = 0;
			Integer bayar = 0;

			for (int i = 0; i < pendaftaranBulk.size(); i++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranBulk.get(i);
				String caraBayar = pendaftaran.get("caraBayar").toString();
				if (caraBayar.equalsIgnoreCase("BPJS")) {
					bpjs++;
				} else if (caraBayar.equalsIgnoreCase("UMUM")) {
					bayar++;
				} else {
					non++;
				}
			}

			dataKunj.setBayar(bayar);
			dataKunj.setBpjs(bpjs);
			dataKunj.setNon(non);
			dataKunj.setTanggal(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));

			URL urlPendaftaranLukas = new URL(poliLukasURL + "getPendaftaranLukasPasienDistinctHariIni");
			JSONArray pendaftaranLukasBulk = getOtherServiceArray(urlPendaftaranLukas);

			for (int i = 0; i < pendaftaranLukasBulk.size(); i++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranLukasBulk.get(i);
				String caraBayar = pendaftaran.get("caraBayar").toString();
				if (caraBayar.equalsIgnoreCase("BPJS")) {
					bpjs++;
				} else if (caraBayar.equalsIgnoreCase("UMUM")) {
					bayar++;
				} else {
					non++;
				}
			}

			dataKunj.setBayar(bayar);
			dataKunj.setBpjs(bpjs);
			dataKunj.setNon(non);
			dataKunj.setTanggal(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}

		return dataKunj;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataPembayaran> getPembayaranByBulan(String tahun, String bulan) {
		List<DataPembayaran> dataBayarList = new ArrayList<>();
		JSONArray tampungBigArray = new JSONArray();
		try {
			URL urlPendaftaran = new URL(pendaftaranV3URL + "getKunjunganPasienDistinctByBulan/" + tahun + "/" + bulan);
			tampungBigArray.add(getOtherServiceArray(urlPendaftaran));

			URL urlPendaftaranLukas = new URL(
					poliLukasURL + "getPendaftaranLukasPasienDistinctByBulan/" + tahun + "/" + bulan);
			tampungBigArray.add(getOtherServiceArray(urlPendaftaranLukas));

			String ambilTanggal = "";

			for (int i = 0; i < tampungBigArray.size(); i++) {
				JSONArray abc = (JSONArray) tampungBigArray.get(i);

				for (int j = 0; j < abc.size(); j++) {
					JSONObject pend = (JSONObject) abc.get(j);

					DataPembayaran dataPem = new DataPembayaran();
					Integer bpjs = 0;
					Integer non = 0;
					Integer bayar = 0;

					try {
						ambilTanggal = pend.get("tanggalPendaftaran").toString();
					} catch (Exception e) {
						ambilTanggal = pend.get("tanggalDaftar").toString();
					}

					boolean found = false;
					for (DataPembayaran tamp : dataBayarList) {
						if (tamp.getTanggal().equalsIgnoreCase(ambilTanggal)) {
							found = true;
							dataPem = tamp;
							break;
						}
					}

					dataPem.setTanggal(ambilTanggal);
					if (found == true) {
						bayar = dataPem.getBayar();
						non = dataPem.getNon();
						bpjs = dataPem.getBpjs();
					}

					String caraBayar = pend.get("caraBayar").toString();
					if (caraBayar.equalsIgnoreCase("BPJS")) {
						bpjs++;
					} else if (caraBayar.equalsIgnoreCase("UMUM")) {
						bayar++;
					} else {
						non++;
					}

					dataPem.setBayar(bayar);
					dataPem.setBpjs(bpjs);
					dataPem.setNon(non);

					if (found == false) {
						dataBayarList.add(dataPem);
					}
				}
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return dataBayarList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataPembayaran> getPembayaranByPeriode(String awal, String akhir) {
		List<DataPembayaran> dataBayarList = new ArrayList<>();
		JSONArray tampungBigArray = new JSONArray();
		try {
			URL urlPendaftaran = new URL(
					pendaftaranV3URL + "getKunjunganPasienDistinctByPeriode/" + awal + "/" + akhir);
			tampungBigArray.add(getOtherServiceArray(urlPendaftaran));

			URL urlPendaftaranLukas = new URL(
					poliLukasURL + "getPendaftaranLukasPasienDistinctByPeriode/" + awal + "/" + akhir);
			tampungBigArray.add(getOtherServiceArray(urlPendaftaranLukas));

			String ambilTanggal = "";

			for (int i = 0; i < tampungBigArray.size(); i++) {
				JSONArray abc = (JSONArray) tampungBigArray.get(i);

				for (int j = 0; j < abc.size(); j++) {
					JSONObject pend = (JSONObject) abc.get(j);

					DataPembayaran dataPem = new DataPembayaran();
					Integer bpjs = 0;
					Integer non = 0;
					Integer bayar = 0;

					try {
						ambilTanggal = pend.get("tanggalPendaftaran").toString();
					} catch (Exception e) {
						ambilTanggal = pend.get("tanggalDaftar").toString();
					}

					boolean found = false;
					for (DataPembayaran tamp : dataBayarList) {
						if (tamp.getTanggal().equalsIgnoreCase(ambilTanggal)) {
							found = true;
							dataPem = tamp;
							break;
						}
					}

					dataPem.setTanggal(ambilTanggal);
					if (found == true) {
						bayar = dataPem.getBayar();
						non = dataPem.getNon();
						bpjs = dataPem.getBpjs();
					}

					String caraBayar = pend.get("caraBayar").toString();
					if (caraBayar.equalsIgnoreCase("BPJS")) {
						bpjs++;
					} else if (caraBayar.equalsIgnoreCase("UMUM")) {
						bayar++;
					} else {
						non++;
					}

					dataPem.setBayar(bayar);
					dataPem.setBpjs(bpjs);
					dataPem.setNon(non);

					if (found == false) {
						dataBayarList.add(dataPem);
					}
				}
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return dataBayarList;
	}

	@Override
	public DataKunjungan getDataKunjunganPasienHariIni() {
		DataKunjungan dataKunj = new DataKunjungan();
		try {
			String jam = "";
			Integer tampJamPagiLama = 0;
			Integer tampJamSiangLama = 0;
			Integer tampJamMalamLama = 0;
			Integer tampJamPagiBaru = 0;
			Integer tampJamSiangBaru = 0;
			Integer tampJamMalamBaru = 0;

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);

			URL urlPendaftaran = new URL(pendaftaranV3URL + "getKunjunganPasienDistinctHariIni");
			JSONArray pendaftaranBulk = getOtherServiceArray(urlPendaftaran);

			for (int i = 0; i < pendaftaranBulk.size(); i++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranBulk.get(i);
				URL urlStatusPasien = new URL(
						pendaftaranV3URL + "getStatusPasienDariKunjungan/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				String stat = objStatus.get("status").toString();

				if (stat.equalsIgnoreCase("false")) {
					jam = pendaftaran.get("waktuCheckin").toString();
					java.util.Date jamAsli = sdf.parse(jam);

					if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
						tampJamPagiLama++;
					} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
						tampJamSiangLama++;
					} else
						tampJamMalamLama++;
				} else {
					jam = pendaftaran.get("waktuCheckin").toString();
					java.util.Date jamAsli = sdf.parse(jam);

					if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
						tampJamPagiBaru++;
					} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
						tampJamSiangBaru++;
					} else
						tampJamMalamBaru++;
				}

				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setPagiJumlah(tampJamPagiBaru + tampJamPagiLama);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setSiangJumlah(tampJamSiangBaru + tampJamSiangLama);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(tampJamMalamBaru + tampJamMalamLama);
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());
				dataKunj.setTanggal(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
			}

			URL urlPendaftaranLukas = new URL(poliLukasURL + "getPendaftaranLukasPasienDistinctHariIni");
			JSONArray pendaftaranLukasBulk = getOtherServiceArray(urlPendaftaranLukas);
			for (int i = 0; i < pendaftaranLukasBulk.size(); i++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranLukasBulk.get(i);
				URL urlStatusPasien = new URL(
						poliLukasURL + "getStatusPasienDariPendaftaranLukas/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				String stat = objStatus.get("status").toString();

				if (stat.equalsIgnoreCase("false")) {
					jam = pendaftaran.get("waktuCheckin").toString();
					java.util.Date jamAsli = sdf.parse(jam);

					if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
						tampJamPagiLama++;
					} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
						tampJamSiangLama++;
					} else
						tampJamMalamLama++;
				} else {
					jam = pendaftaran.get("waktuCheckin").toString();
					java.util.Date jamAsli = sdf.parse(jam);

					if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
						tampJamPagiBaru++;
					} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
						tampJamSiangBaru++;
					} else
						tampJamMalamBaru++;
				}

				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setPagiJumlah(tampJamPagiBaru + tampJamPagiLama);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setSiangJumlah(tampJamSiangBaru + tampJamSiangLama);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(tampJamMalamBaru + tampJamMalamLama);
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());
				dataKunj.setTanggal(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}

		return dataKunj;
	}

	@Override
	public List<DataKunjungan> getDataKunjunganPasienByBulan(String tahun, String bulan) {
		List<DataKunjungan> tamp = new ArrayList<>();
		String tanggal = "";
		String jam = "";
		String statusPasien = "";
		try {
			List<List<String>> array = new ArrayList<List<String>>();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);
			String ambilTanggal = "";
			String tampStatus = "";

			URL urlPendaftaran = new URL(pendaftaranV3URL + "getKunjunganPasienDistinctByBulan/" + tahun + "/" + bulan);
			JSONArray pendaftaranBulk = getOtherServiceArray(urlPendaftaran);

			for (int a = 0; a < pendaftaranBulk.size(); a++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranBulk.get(a);
				jam = pendaftaran.get("waktuCheckin").toString();
				tanggal = pendaftaran.get("tanggalPendaftaran").toString();

				URL urlStatusPasien = new URL(
						pendaftaranV3URL + "getStatusPasienDariKunjungan/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				statusPasien = objStatus.get("status").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(tanggal)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			URL urlPendaftaranLukas = new URL(
					poliLukasURL + "getPendaftaranLukasPasienDistinctByBulan/" + tahun + "/" + bulan);
			JSONArray pendaftaranLukasBulk = getOtherServiceArray(urlPendaftaranLukas);

			for (int a = 0; a < pendaftaranLukasBulk.size(); a++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranLukasBulk.get(a);
				jam = pendaftaran.get("waktuCheckin").toString();
				tanggal = pendaftaran.get("tanggalDaftar").toString();

				URL urlStatusPasien = new URL(
						poliLukasURL + "getStatusPasienDariPendaftaranLukas/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				statusPasien = objStatus.get("status").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(tanggal)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			ambilTanggal = "";
			String tanggalLama = "";
			for (int i = 0; i < array.size(); i++) {
				List<String> potongArray = array.get(i);
				DataKunjungan dataKunj = new DataKunjungan();
				Integer tampJamPagiBaru = 0;
				Integer tampJamSiangBaru = 0;
				Integer tampJamMalamBaru = 0;
				Integer tampJamPagiLama = 0;
				Integer tampJamSiangLama = 0;
				Integer tampJamMalamLama = 0;
				ambilTanggal = potongArray.get(0);

				if (tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					dataKunj = tamp.get(tamp.size() - 1);
					tampJamPagiBaru = dataKunj.getPagiBaru();
					tampJamSiangBaru = dataKunj.getSiangBaru();
					tampJamMalamBaru = dataKunj.getMalamBaru();
					tampJamPagiLama = dataKunj.getPagiLama();
					tampJamSiangLama = dataKunj.getSiangLama();
					tampJamMalamLama = dataKunj.getMalamLama();
				}

				for (int j = 0; j < potongArray.size(); j++) {
					dataKunj.setTanggal(ambilTanggal);
					tampStatus = potongArray.get(1);
					if (tampStatus.equalsIgnoreCase("false")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiLama++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangLama++;
							} else {
								tampJamMalamLama++;
							}
						}

					} else if (tampStatus.equalsIgnoreCase("true")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiBaru++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangBaru++;
							} else {
								tampJamMalamBaru++;
							}
						}
					}
				}
				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(dataKunj.getMalamBaru() + dataKunj.getMalamLama());
				dataKunj.setPagiJumlah(dataKunj.getPagiBaru() + dataKunj.getPagiLama());
				dataKunj.setSiangJumlah(dataKunj.getSiangBaru() + dataKunj.getSiangLama());
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());

				if (!tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					tanggalLama = ambilTanggal;
					tamp.add(dataKunj);
				}
			}

		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYSELECT.getCode());
		}
		return tamp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getDataKunjunganPasienByBulanTersortir(String tahun, String bulan) {
		JSONObject tamp = new JSONObject();
		JSONArray libur = new JSONArray();
		JSONArray kerja = new JSONArray();

		List<TanggalLibur> tanggalLbr = getTanggalLibur(tahun, bulan);
		List<DataKunjungan> kunjunganPasien = getDataKunjunganPasienByBulan(tahun, bulan);

		if (kunjunganPasien.size() < tanggalLbr.size()) {
			throw new MrException("Tanggal Libur lebih banyak dari Tanggal Kunjungan",
					StatusCode.DATANOTFOUND.getCode());
		}

		String tglPasien = "";
		String tglLbr = "";

		// tgl libur 3, 6
		// data kunjungan 3, 6, 7
		// data kunjungan harus > tgl libur

		for (DataKunjungan pas : kunjunganPasien) {
			boolean found = false;
			DataKunjungan tampz = new DataKunjungan();

			for (TanggalLibur lbr : tanggalLbr) {
				tampz = pas;
				tglPasien = pas.getTanggal();
				tglLbr = lbr.getTanggal();

				if (tglPasien.equalsIgnoreCase(tglLbr)) {
					libur.add(tampz);
					found = true;
					break;
				} else {
					found = false;
				}
			}

			if (found == false) {
				kerja.add(tampz);
				System.out.println("kerja");
			}

		}
		tamp.put("libur", libur);
		tamp.put("kerja", kerja);

		return tamp;
	}

	@Override
	public List<DataKunjungan> getDataKunjunganPasienByPeriode(String awal, String akhir) {
		List<DataKunjungan> tamp = new ArrayList<>();
		String tanggal = "";
		String jam = "";
		String statusPasien = "";
		try {
			List<List<String>> array = new ArrayList<List<String>>();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);
			String ambilTanggal = "";
			String tampStatus = "";

			URL urlPendaftaran = new URL(
					pendaftaranV3URL + "getKunjunganPasienDistinctByPeriode/" + awal + "/" + akhir);
			JSONArray pendaftaranBulk = getOtherServiceArray(urlPendaftaran);

			for (int a = 0; a < pendaftaranBulk.size(); a++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranBulk.get(a);
				jam = pendaftaran.get("waktuCheckin").toString();
				tanggal = pendaftaran.get("tanggalPendaftaran").toString();

				URL urlStatusPasien = new URL(
						pendaftaranV3URL + "getStatusPasienDariKunjungan/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				statusPasien = objStatus.get("status").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(tanggal)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			URL urlPendaftaranLukas = new URL(
					poliLukasURL + "getPendaftaranLukasPasienDistinctByPeriode/" + awal + "/" + akhir);
			JSONArray pendaftaranLukasBulk = getOtherServiceArray(urlPendaftaranLukas);

			for (int a = 0; a < pendaftaranLukasBulk.size(); a++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranLukasBulk.get(a);
				jam = pendaftaran.get("waktuCheckin").toString();
				tanggal = pendaftaran.get("tanggalDaftar").toString();

				URL urlStatusPasien = new URL(
						poliLukasURL + "getStatusPasienDariPendaftaranLukas/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				statusPasien = objStatus.get("status").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(tanggal)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			ambilTanggal = "";
			String tanggalLama = "";
			for (int i = 0; i < array.size(); i++) {
				List<String> potongArray = array.get(i);
				DataKunjungan dataKunj = new DataKunjungan();
				Integer tampJamPagiBaru = 0;
				Integer tampJamSiangBaru = 0;
				Integer tampJamMalamBaru = 0;
				Integer tampJamPagiLama = 0;
				Integer tampJamSiangLama = 0;
				Integer tampJamMalamLama = 0;
				ambilTanggal = potongArray.get(0);

				if (tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					dataKunj = tamp.get(tamp.size() - 1);
					tampJamPagiBaru = dataKunj.getPagiBaru();
					tampJamSiangBaru = dataKunj.getSiangBaru();
					tampJamMalamBaru = dataKunj.getMalamBaru();
					tampJamPagiLama = dataKunj.getPagiLama();
					tampJamSiangLama = dataKunj.getSiangLama();
					tampJamMalamLama = dataKunj.getMalamLama();
				}

				for (int j = 0; j < potongArray.size(); j++) {
					dataKunj.setTanggal(ambilTanggal);
					tampStatus = potongArray.get(1);
					if (tampStatus.equalsIgnoreCase("false")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiLama++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangLama++;
							} else {
								tampJamMalamLama++;
							}
						}

					} else if (tampStatus.equalsIgnoreCase("true")) {
						if (j != 0 && j != 1) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiBaru++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangBaru++;
							} else {
								tampJamMalamBaru++;
							}
						}
					}
				}
				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(dataKunj.getMalamBaru() + dataKunj.getMalamLama());
				dataKunj.setPagiJumlah(dataKunj.getPagiBaru() + dataKunj.getPagiLama());
				dataKunj.setSiangJumlah(dataKunj.getSiangBaru() + dataKunj.getSiangLama());
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());

				if (!tanggalLama.equalsIgnoreCase(ambilTanggal)) {
					tanggalLama = ambilTanggal;
					tamp.add(dataKunj);
				}
			}

		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYSELECT.getCode());
		}
		return tamp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getDataKunjunganPasienByPeriodeTersortir(String awal, String akhir) {
		JSONObject tamp = new JSONObject();
		JSONArray libur = new JSONArray();
		JSONArray kerja = new JSONArray();

		List<TanggalLibur> tanggalLbr = getTanggalLiburByPeriode(awal, akhir);
		List<DataKunjungan> kunjunganPasien = getDataKunjunganPasienByPeriode(awal, akhir);

		if (kunjunganPasien.size() < tanggalLbr.size()) {
			throw new MrException("Tanggal Libur lebih banyak dari Tanggal Kunjungan",
					StatusCode.DATANOTFOUND.getCode());
		}

		String tglPasien = "";
		String tglLbr = "";

		// tgl libur 3, 6
		// data kunjungan 3, 6, 7
		// data kunjungan harus > tgl libur

		for (DataKunjungan pas : kunjunganPasien) {
			boolean found = false;
			DataKunjungan tampz = new DataKunjungan();

			for (TanggalLibur lbr : tanggalLbr) {
				tampz = pas;
				tglPasien = pas.getTanggal();
				tglLbr = lbr.getTanggal();

				if (tglPasien.equalsIgnoreCase(tglLbr)) {
					libur.add(tampz);
					found = true;
					break;
				} else {
					found = false;
				}
			}

			if (found == false) {
				kerja.add(tampz);
			}

		}
		tamp.put("libur", libur);
		tamp.put("kerja", kerja);

		return tamp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getJumlahDaftarKunjunganPasienHariIni() {
		JSONArray tamp = new JSONArray();
		JSONObject jumlah = new JSONObject();
		Integer hitung = 0;
		try {
			URL urlPendaftaran = new URL(pendaftaranV3URL + "getJumlahDaftarDistinctHariIni");
			tamp.add(getOtherServiceArray(urlPendaftaran));

			URL urlPendaftaranLukas = new URL(poliLukasURL + "getJumlahPendaftaranLukasPasienDistinctHariIni");
			tamp.add(getOtherServiceArray(urlPendaftaranLukas));

			for (int i = 0; i < tamp.size(); i++) {
				JSONArray abc = (JSONArray) tamp.get(i);

				for (int j = 0; j < abc.size(); j++) {
					JSONObject pend = (JSONObject) abc.get(j);
					hitung++;
				}
			}

			jumlah.put("jumlah", hitung);

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return jumlah;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataKunjungan> getJumlahDaftarKunjunganPasienByBulan(String tahun, String bulan) {
		JSONArray tamp = new JSONArray();
		List<DataKunjungan> dataKunjList = new ArrayList<>();

		try {
			URL urlPendaftaran = new URL(pendaftaranV3URL + "getJumlahDaftarDistinctByBulan/" + tahun + "/" + bulan);
			tamp.add(getOtherServiceArray(urlPendaftaran));

			URL urlPendaftaranLukas = new URL(
					poliLukasURL + "getJumlahPendaftaranLukasPasienDistinctByBulan/" + tahun + "/" + bulan);
			tamp.add(getOtherServiceArray(urlPendaftaranLukas));

			String ambilTanggal = "";
			Integer total = 0;
			for (int i = 0; i < tamp.size(); i++) {
				JSONArray abc = (JSONArray) tamp.get(i);

				for (int j = 0; j < abc.size(); j++) {
					JSONObject pend = (JSONObject) abc.get(j);

					DataKunjungan dataKunj = new DataKunjungan();

					try {
						ambilTanggal = pend.get("tanggalPendaftaran").toString();
					} catch (Exception e) {
						ambilTanggal = pend.get("tanggalDaftar").toString();
					}

					boolean found = false;
					for (DataKunjungan tampung : dataKunjList) {
						if (tampung.getTanggal().equalsIgnoreCase(ambilTanggal)) {
							found = true;
							dataKunj = tampung;
							total = dataKunj.getTotalSemua();
							total++;
							break;
						}
					}

					if (found == true) {
						dataKunj.setTotalSemua(total);
					} else if (found == false) {
						dataKunj.setTanggal(ambilTanggal);
						total = 0;
						total++;
						dataKunj.setTotalSemua(total);
						dataKunjList.add(dataKunj);
					}

				}
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return dataKunjList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataKunjungan> getJumlahDaftarKunjunganPasienByPeriode(String awal, String akhir) {
		JSONArray tamp = new JSONArray();
		List<DataKunjungan> dataKunjList = new ArrayList<>();

		try {
			URL urlPendaftaran = new URL(pendaftaranV3URL + "getJumlahDaftarDistinctByPeriode/" + awal + "/" + akhir);
			tamp.add(getOtherServiceArray(urlPendaftaran));

			URL urlPendaftaranLukas = new URL(
					poliLukasURL + "getJumlahPendaftaranLukasPasienDistinctByPeriode/" + awal + "/" + akhir);
			tamp.add(getOtherServiceArray(urlPendaftaranLukas));

			String ambilTanggal = "";
			Integer total = 0;
			for (int i = 0; i < tamp.size(); i++) {
				JSONArray abc = (JSONArray) tamp.get(i);

				for (int j = 0; j < abc.size(); j++) {
					JSONObject pend = (JSONObject) abc.get(j);

					DataKunjungan dataKunj = new DataKunjungan();

					try {
						ambilTanggal = pend.get("tanggalPendaftaran").toString();
					} catch (Exception e) {
						ambilTanggal = pend.get("tanggalDaftar").toString();
					}

					boolean found = false;
					for (DataKunjungan tampung : dataKunjList) {
						if (tampung.getTanggal().equalsIgnoreCase(ambilTanggal)) {
							found = true;
							dataKunj = tampung;
							total = dataKunj.getTotalSemua();
							total++;
							break;
						}
					}

					if (found == true) {
						dataKunj.setTotalSemua(total);
					} else if (found == false) {
						dataKunj.setTanggal(ambilTanggal);
						total = 0;
						total++;
						dataKunj.setTotalSemua(total);
						dataKunjList.add(dataKunj);
					}

				}
			}

		} catch (Exception e) {
			throw new MrException("", "Data Tidak Ditemukan", StatusCode.DATANOTFOUND.getCode());
		}
		return dataKunjList;
	}

	@Override
	public List<DataKunjungan> getDataPoliklinikSpesialisDariRegulerByBulan(String tahun, String bulan) {
		List<DataKunjungan> tamp = new ArrayList<>();
		String tanggal = "";
		String jam = "";
		String statusPasien = "";
		String klinikDokterId = null;

		try {
			List<List<String>> array = new ArrayList<List<String>>();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String batas1 = "06:59:59";
			String batas2 = "13:59:59";
			String batas3 = "20:59:59";
			java.util.Date parseBatas1 = sdf.parse(batas1);
			java.util.Date parseBatas2 = sdf.parse(batas2);
			java.util.Date parseBatas3 = sdf.parse(batas3);
			String ambilDokter = "";
			String ambilSpesialisasi = "";
			String tampStatus = "";

			URL urlPendaftaran = new URL(pendaftaranV3URL + "getKunjunganPasienDistinctByBulan/" + tahun + "/" + bulan);
			JSONArray pendaftaranBulk = getOtherServiceArray(urlPendaftaran);

			for (int a = 0; a < pendaftaranBulk.size(); a++) {
				JSONObject pendaftaran = (JSONObject) pendaftaranBulk.get(a);
				jam = pendaftaran.get("waktuCheckin").toString();
				tanggal = pendaftaran.get("tanggalPendaftaran").toString();
				klinikDokterId = pendaftaran.get("klinikDokterId").toString();

				URL urlStatusPasien = new URL(
						pendaftaranV3URL + "getStatusPasienDariKunjungan/" + pendaftaran.get("pasienId").toString());
				JSONObject objStatus = getOtherService(urlStatusPasien);
				statusPasien = objStatus.get("status").toString();

				if (array.isEmpty()) {
					array.add(new ArrayList<String>());
					array.get(0).add(tanggal);
					array.get(0).add(statusPasien);
					array.get(0).add(klinikDokterId);
					array.get(0).add(jam);
				} else {
					boolean found = true;
					for (int i = 0; i < array.size(); i++) {
						if (array.get(i).contains(klinikDokterId)) {
							if (array.get(i).contains(statusPasien)) {
								array.get(i).add(jam);
								found = true;
								break;
							}
						} else {
							found = false;
						}
					}
					if (found == false) {
						array.add(new ArrayList<String>());
						array.get(array.size() - 1).add(tanggal);
						array.get(array.size() - 1).add(statusPasien);
						array.get(array.size() - 1).add(klinikDokterId);
						array.get(array.size() - 1).add(jam);
					}
				}
			}

			ambilDokter = "";
			String dokterLama = "";
			ambilSpesialisasi = "";
			String spesialisasiLama = "";
			boolean found = false;

			for (int i = 0; i < array.size(); i++) {
				List<String> potongArray = array.get(i);
				DataKunjungan dataKunj = new DataKunjungan();
				Integer tampJamPagiBaru = 0;
				Integer tampJamSiangBaru = 0;
				Integer tampJamMalamBaru = 0;
				Integer tampJamPagiLama = 0;
				Integer tampJamSiangLama = 0;
				Integer tampJamMalamLama = 0;
				ambilDokter = potongArray.get(2);
				System.out.println(ambilDokter);

				URL urlJadwal = new URL(
						jadwalURL + "LihatPoliDanSpesialisDanSubspesialisbyIdKlinikDokter/" + ambilDokter);
				JSONObject jadwalDokter = getOtherService(urlJadwal);
				JSONObject detilDokter = (JSONObject) jadwalDokter.get("DetailDokter");
				String namaSpesialisasi = detilDokter.get("namaSpesialisasi").toString();
				ambilSpesialisasi = namaSpesialisasi;
				System.out.println(ambilSpesialisasi);

				if (spesialisasiLama.equalsIgnoreCase(ambilSpesialisasi)) {
					for (DataKunjungan kunj : tamp) {
						if (kunj.getTampungan2().equalsIgnoreCase(ambilSpesialisasi)) {
							dataKunj = kunj;
							tampJamPagiBaru = dataKunj.getPagiBaru();
							tampJamSiangBaru = dataKunj.getSiangBaru();
							tampJamMalamBaru = dataKunj.getMalamBaru();
							tampJamPagiLama = dataKunj.getPagiLama();
							tampJamSiangLama = dataKunj.getSiangLama();
							tampJamMalamLama = dataKunj.getMalamLama();
							found = true;
							// break;
						} else {
							found = false;
						}
					}
				}

				for (int j = 0; j < potongArray.size(); j++) {
					dataKunj.setTampungan(ambilDokter);
					dataKunj.setTampungan2(namaSpesialisasi);
					tampStatus = potongArray.get(1);
					if (tampStatus.equalsIgnoreCase("false")) {
						if (j != 0 && j != 1 && j != 2) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiLama++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangLama++;
							} else {
								tampJamMalamLama++;
							}
						}

					} else if (tampStatus.equalsIgnoreCase("true")) {
						if (j != 0 && j != 1 && j != 2) {
							String ambilJam = potongArray.get(j);
							java.util.Date jamAsli = sdf.parse(ambilJam);
							if (jamAsli.after(parseBatas1) && jamAsli.before(parseBatas2)) {
								tampJamPagiBaru++;
							} else if (jamAsli.after(parseBatas2) && jamAsli.before(parseBatas3)) {
								tampJamSiangBaru++;
							} else {
								tampJamMalamBaru++;
							}
						}
					}
				}
				dataKunj.setPagiBaru(tampJamPagiBaru);
				dataKunj.setSiangBaru(tampJamSiangBaru);
				dataKunj.setMalamBaru(tampJamMalamBaru);
				dataKunj.setPagiLama(tampJamPagiLama);
				dataKunj.setSiangLama(tampJamSiangLama);
				dataKunj.setMalamLama(tampJamMalamLama);
				dataKunj.setMalamJumlah(dataKunj.getMalamBaru() + dataKunj.getMalamLama());
				dataKunj.setPagiJumlah(dataKunj.getPagiBaru() + dataKunj.getPagiLama());
				dataKunj.setSiangJumlah(dataKunj.getSiangBaru() + dataKunj.getSiangLama());
				dataKunj.setTotalBaru(dataKunj.getPagiBaru() + dataKunj.getSiangBaru() + dataKunj.getMalamBaru());
				dataKunj.setTotalLama(dataKunj.getPagiLama() + dataKunj.getSiangLama() + dataKunj.getMalamLama());
				dataKunj.setTotalSemua(dataKunj.getTotalBaru() + dataKunj.getTotalLama());

				// if (!spesialisasiLama.equalsIgnoreCase(ambilSpesialisasi)) {
				// spesialisasiLama = ambilSpesialisasi;
				// tamp.add(dataKunj);
				// }
				if (found == false) {
					spesialisasiLama = ambilSpesialisasi;
					tamp.add(dataKunj);
				}
			}
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYSELECT.getCode());
		}
		return tamp;
	}

	@Override
	public Boolean getStatusKunjunganByPasienId(Integer pasienId) {
		Boolean tamp = false;
		// false untuk tidak ditemukan
		// true jika ditemukan
		tamp = iMrDAO.getStatusKunjunganByPasienId(pasienId);

		return tamp;
	}

	@Override
	public void tambahStatusKunjungan(StatusKunjungan statusKunjungan) {
		try {
			iMrDAO.tambahStatusKunjungan(statusKunjungan);
		} catch (Exception e) {
			throw new MrException(e.getMessage(), "", StatusCode.QUERYINSERT.getCode());
		}
	}

	@Override
	public WrapperPenggabunganRm penggabunganNoRm(WrapperPenggabunganRm penggabunganRm) {

		String rmAsal = penggabunganRm.getNoRmAsal();
		String rmTujuan = penggabunganRm.getNoRmTujuan();
		String namaKel = penggabunganRm.getKelompok();
		String tanggalPeriksa = penggabunganRm.getTanggalPeriksa();
		String keterangan = "";

		switch (namaKel) {

		case "Poli Reguler":
			try {
				URL urlPendaftaran = new URL(pendaftaranV3URL + "penggabunganNoRmPoliReguler/" + rmAsal + "/" + rmTujuan
						+ "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlPendaftaran.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Poli Reguler";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Poli Reguler";
			}
			break;

		case "Poli Lukas":
			try {
				URL urlLukas = new URL(
						poliLukasURL + "penggabunganNoRmPoliLukas/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlLukas.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Poli Lukas";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Poli Lukas";
			}
			break;

		case "Elektromedis":
			try {
				URL urlElektro = new URL(
						elektroURL + "penggabunganNoRmElektromedis/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlElektro.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Elektromedis";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Elektromedis";
			}
			break;

		case "Hemodialisa":
			try {
				URL urlHemodialisa = new URL(
						hemodURL + "penggabunganNoRmHemodialisa/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlHemodialisa.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Hemodialisa";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Hemodialisa";
			}
			break;

		case "Laporan Operasi":
			try {
				URL urlElektro = new URL(elektroURL + "penggabunganNoRmLaporanOperasi/" + rmAsal + "/" + rmTujuan + "/"
						+ tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlElektro.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Laporan Operasi";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Laporan Operasi";
			}
			break;

		case "Fisioterapi":
			try {
				URL urlFisio = new URL(pendaftaranV3URL + "penggabunganNoRmFisioterapi/" + rmAsal + "/" + rmTujuan + "/"
						+ tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlFisio.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Fisioterapi";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Fisioterapi";
			}
			break;

		case "Radiologi":
			try {
				URL urlRadio = new URL(
						radiolURL + "penggabunganNoRmRadiologi/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlRadio.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Radiologi";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Radiologi";
			}
			break;

		case "Laboratorium":
			try {
				URL urlLab = new URL(labURL + "penggabunganNoRmLab/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlLab.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Laboratorium";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Laboratorium";
			}
			break;

		case "Farmasi":
			try {
				URL urlFarmasi = new URL(
						farmasiURL + "penggabunganNoRmFarmasi/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlFarmasi.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Farmasi";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Farmasi";
			}
			break;

		case "Keuangan":
			try {
				URL urlBank = new URL(
						bankURL + "penggabunganNoRmBank/" + rmAsal + "/" + rmTujuan + "/" + tanggalPeriksa);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(urlBank.toString(), String.class);
				if (result.equalsIgnoreCase("true")) {
					keterangan = "Berhasil merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Bank";
				} else {
					throw new MrException("", "", StatusCode.GETOHERSERVICE.getCode());
				}
			} catch (Exception e) {
				keterangan = "Gagal merubah noRM" + rmAsal + " menjadi" + rmTujuan + " di Bank";
			}
			break;
		default:
			keterangan = "Gagal merubah noRM";
		}

		penggabunganRm.setKeterangan(keterangan);
		return penggabunganRm;
	}
	
}
