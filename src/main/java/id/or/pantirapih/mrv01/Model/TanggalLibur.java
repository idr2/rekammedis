package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tanggallibur")
public class TanggalLibur implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long idTanggalLibur;
	@Column(name = "hari")
	private String hari;
	@Column(name = "bulan")
	private String bulan;
	@Column(name = "tanggal")
	private String tanggal;
	@Column(name = "tahun")
	private String tahun;
	@Column(name = "adddate")
	private String addDate;
	@Column(name = "status")
	private Boolean status;
	
	public Long getIdTanggalLibur() {
		return idTanggalLibur;
	}
	public void setIdTanggalLibur(Long idTanggalLibur) {
		this.idTanggalLibur = idTanggalLibur;
	}
	public String getHari() {
		return hari;
	}
	public void setHari(String hari) {
		this.hari = hari;
	}
	public String getBulan() {
		return bulan;
	}
	public void setBulan(String bulan) {
		this.bulan = bulan;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getTahun() {
		return tahun;
	}
	public void setTahun(String tahun) {
		this.tahun = tahun;
	}
	public String getAddDate() {
		return addDate;
	}
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
