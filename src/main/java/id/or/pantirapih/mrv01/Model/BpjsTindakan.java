package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bpjstindakan")
public class BpjsTindakan implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long idBpjsTindakan;
	@Column(name = "bpjsolah_id")
	private Integer bpjsOlahId;
	@Column(name = "kodetindakan")
	private String kodeTindakan;
	@Column(name = "namatindakan")
	private String namaTindakan;
	@Column(name = "adddate")
	private String addDate;
	@Column(name = "updatedate")
	private String updateDate;
	@Column(name = "deleted")
	private Boolean deleted;
	
	public Long getIdBpjsTindakan() {
		return idBpjsTindakan;
	}
	public void setIdBpjsTindakan(Long idBpjsTindakan) {
		this.idBpjsTindakan = idBpjsTindakan;
	}
	public Integer getBpjsOlahId() {
		return bpjsOlahId;
	}
	public void setBpjsOlahId(Integer bpjsOlahId) {
		this.bpjsOlahId = bpjsOlahId;
	}
	public String getKodeTindakan() {
		return kodeTindakan;
	}
	public void setKodeTindakan(String kodeTindakan) {
		this.kodeTindakan = kodeTindakan;
	}
	public String getNamaTindakan() {
		return namaTindakan;
	}
	public void setNamaTindakan(String namaTindakan) {
		this.namaTindakan = namaTindakan;
	}
	public String getAddDate() {
		return addDate;
	}
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
