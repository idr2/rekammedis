package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "statuskunjungan")
public class StatusKunjungan implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long idStatusKunjungan;
	@Column(name = "pasien_id")
	private Integer pasienId;
	@Column(name = "poli")
	private String poli;
	@Column(name = "jumlahkunjungan")
	private Integer jumlahKunjungan;
	@Column(name = "adddate")
	private String addDate;
	
	public Long getIdStatusKunjungan() {
		return idStatusKunjungan;
	}
	public void setIdStatusKunjungan(Long idStatusKunjungan) {
		this.idStatusKunjungan = idStatusKunjungan;
	}
	public Integer getPasienId() {
		return pasienId;
	}
	public void setPasienId(Integer pasienId) {
		this.pasienId = pasienId;
	}
	public String getPoli() {
		return poli;
	}
	public void setPoli(String poli) {
		this.poli = poli;
	}
	public Integer getJumlahKunjungan() {
		return jumlahKunjungan;
	}
	public void setJumlahKunjungan(Integer jumlahKunjungan) {
		this.jumlahKunjungan = jumlahKunjungan;
	}
	public String getAddDate() {
		return addDate;
	}
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
