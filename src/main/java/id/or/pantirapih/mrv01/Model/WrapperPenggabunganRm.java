package id.or.pantirapih.mrv01.Model;

public class WrapperPenggabunganRm {
    public String noRmAsal;
    public String noRmTujuan;
    public String tanggalPeriksa;
    public String kelompok;
    public String keterangan;


    public String getNoRmAsal() {
        return this.noRmAsal;
    }

    public void setNoRmAsal(String noRmAsal) {
        this.noRmAsal = noRmAsal;
    }

    public String getNoRmTujuan() {
        return this.noRmTujuan;
    }

    public void setNoRmTujuan(String noRmTujuan) {
        this.noRmTujuan = noRmTujuan;
    }

    public String getTanggalPeriksa() {
        return this.tanggalPeriksa;
    }

    public void setTanggalPeriksa(String tanggalPeriksa) {
        this.tanggalPeriksa = tanggalPeriksa;
    }

    public String getKelompok() {
        return this.kelompok;
    }

    public void setKelompok(String kelompok) {
        this.kelompok = kelompok;
    }

    public String getKeterangan() {
        return this.keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
  
}