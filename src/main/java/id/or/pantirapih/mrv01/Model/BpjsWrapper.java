package id.or.pantirapih.mrv01.Model;

import java.util.List;

public class BpjsWrapper {
	private BpjsOlahData bpjsOlahData;
	private List<BpjsDiagnosa> bpjsDiagnosa;
	private List<BpjsTindakan> bpjsTindakan;
	
	public BpjsOlahData getBpjsOlahData() {
		return bpjsOlahData;
	}
	public void setBpjsOlahData(BpjsOlahData bpjsOlahData) {
		this.bpjsOlahData = bpjsOlahData;
	}
	public List<BpjsDiagnosa> getBpjsDiagnosa() {
		return bpjsDiagnosa;
	}
	public void setBpjsDiagnosa(List<BpjsDiagnosa> bpjsDiagnosa) {
		this.bpjsDiagnosa = bpjsDiagnosa;
	}
	public List<BpjsTindakan> getBpjsTindakan() {
		return bpjsTindakan;
	}
	public void setBpjsTindakan(List<BpjsTindakan> bpjsTindakan) {
		this.bpjsTindakan = bpjsTindakan;
	}
}
