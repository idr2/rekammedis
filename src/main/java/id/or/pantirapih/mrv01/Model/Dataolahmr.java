package id.or.pantirapih.mrv01.Model;

import java.util.List;


public class Dataolahmr {
	private Olahmr olahmr;
	private List<Diagnosa> diagnosa;
	private List<Tindakan> tindakan;
	public Olahmr getOlahmr() {
		return olahmr;
	}
	public void setOlahmr(Olahmr olahmr) {
		this.olahmr = olahmr;
	}
	public List<Diagnosa> getDiagnosa() {
		return diagnosa;
	}
	public void setDiagnosa(List<Diagnosa> diagnosa) {
		this.diagnosa = diagnosa;
	}
	public List<Tindakan> getTindakan() {
		return tindakan;
	}
	public void setTindakan(List<Tindakan> tindakan) {
		this.tindakan = tindakan;
	}
}
