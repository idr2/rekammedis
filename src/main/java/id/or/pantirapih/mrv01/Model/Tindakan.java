package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tindakan")
public class Tindakan implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "`id`") public Integer idTindakan;
	@Column(name = "`idolahmr`") public Integer idOlahMr;
	@Column(name = "`kodetindakan`") public String kodeTindakan;
	@Column(name = "`namatindakan`") public String namaTindakan;
	@Column(name = "`deleted`") public String deleted;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="idOlahMr", referencedColumnName="id",insertable = false, updatable = false)



	public Integer getIdTindakan() {
		return idTindakan;
	}
	public Integer getIdOlahMr() {
		return idOlahMr;
	}
	public void setIdOlahMr(Integer idOlahMr) {
		this.idOlahMr = idOlahMr;
	}
	public void setIdTindakan(Integer idTindakan) {
		this.idTindakan = idTindakan;
	}
	public String getKodeTindakan() {
		return kodeTindakan;
	}
	public void setKodeTindakan(String kodeTindakan) {
		this.kodeTindakan = kodeTindakan;
	}
	public String getNamaTindakan() {
		return namaTindakan;
	}
	public void setNamaTindakan(String namaTindakan) {
		this.namaTindakan = namaTindakan;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

}


