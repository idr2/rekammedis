package id.or.pantirapih.mrv01.Model;

public class DataPasien {

	private Integer pagiBaru;
	private Integer pagiLama;
	private Integer pagiJumlah;
	private Integer siangBaru;
	private Integer siangLama;
	private Integer siangJumlah;
	private Integer malamBaru;
	private Integer malamLama;
	private Integer malamJumlah;
	private Integer totalLama;
	private Integer totalBaru;
	private Integer totalSemua;
	private String tanggal;
	
	public Integer getPagiBaru() {
		return pagiBaru;
	}
	public void setPagiBaru(Integer pagiBaru) {
		this.pagiBaru = pagiBaru;
	}
	public Integer getPagiLama() {
		return pagiLama;
	}
	public void setPagiLama(Integer pagiLama) {
		this.pagiLama = pagiLama;
	}
	public Integer getPagiJumlah() {
		return pagiJumlah;
	}
	public void setPagiJumlah(Integer pagiJumlah) {
		this.pagiJumlah = pagiJumlah;
	}
	public Integer getSiangBaru() {
		return siangBaru;
	}
	public void setSiangBaru(Integer siangBaru) {
		this.siangBaru = siangBaru;
	}
	public Integer getSiangLama() {
		return siangLama;
	}
	public void setSiangLama(Integer siangLama) {
		this.siangLama = siangLama;
	}
	public Integer getSiangJumlah() {
		return siangJumlah;
	}
	public void setSiangJumlah(Integer siangJumlah) {
		this.siangJumlah = siangJumlah;
	}
	public Integer getMalamBaru() {
		return malamBaru;
	}
	public void setMalamBaru(Integer malamBaru) {
		this.malamBaru = malamBaru;
	}
	public Integer getMalamLama() {
		return malamLama;
	}
	public void setMalamLama(Integer malamLama) {
		this.malamLama = malamLama;
	}
	public Integer getMalamJumlah() {
		return malamJumlah;
	}
	public void setMalamJumlah(Integer malamJumlah) {
		this.malamJumlah = malamJumlah;
	}
	public Integer getTotalLama() {
		return totalLama;
	}
	public void setTotalLama(Integer totalLama) {
		this.totalLama = totalLama;
	}
	public Integer getTotalBaru() {
		return totalBaru;
	}
	public void setTotalBaru(Integer totalBaru) {
		this.totalBaru = totalBaru;
	}
	public Integer getTotalSemua() {
		return totalSemua;
	}
	public void setTotalSemua(Integer totalSemua) {
		this.totalSemua = totalSemua;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	
}
