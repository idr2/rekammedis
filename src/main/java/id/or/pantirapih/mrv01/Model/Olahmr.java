package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "olahmr")
public class Olahmr implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "`id`") public int idOlahMr ;
	@Column(name = "`idkunjungan`") public String idKunjungan;
	@Column(name = "`noregister`") public String noRegister;
	@Column(name = "`nomr`") public String noMr;
	@Column(name = "`namapasien`") public String namaPasien;
	@Column(name = "`tanggalperiksa`") public Date tanggalPeriksa;
	@Column(name = "`iddokter`") public String idDokter;
	@Column(name = "`tglsave`") public String tglSave;
	@Column(name = "`tglupdate`") public String tglUpdate;
	@Column(name = "`status`") public String status;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public int getIdOlahMr() {
		return idOlahMr;
	}
	public void setIdOlahMr(int idOlahMr) {
		this.idOlahMr = idOlahMr;
	}
	public String getIdKunjungan() {
		return idKunjungan;
	}
	public void setIdKunjungan(String idKunjungan) {
		this.idKunjungan = idKunjungan;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getNoMr() {
		return noMr;
	}
	public void setNoMr(String noMr) {
		this.noMr = noMr;
	}
	public String getNamaPasien() {
		return namaPasien;
	}
	public void setNamaPasien(String namaPasien) {
		this.namaPasien = namaPasien;
	}

	public Date getTanggalPeriksa() {
		return tanggalPeriksa;
	}
	public void setTanggalPeriksa(Date tanggalPeriksa) {
		this.tanggalPeriksa = tanggalPeriksa;
	}
	public String getIdDokter() {
		return idDokter;
	}
	public void setIdDokter(String idDokter) {
		this.idDokter = idDokter;
	}
	public String getTglSave() {
		return tglSave;
	}
	public void setTglSave(String tglSave) {
		this.tglSave = tglSave;
	}
	public String getTglUpdate() {
		return tglUpdate;
	}
	public void setTglUpdate(String tglUpdate) {
		this.tglUpdate = tglUpdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
