package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "diagnosa")
public class Diagnosa implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "`id`") public Integer idDiagnosaOlahMr;
	@Column(name = "`idolahmr`") public Integer idOlahMr ;
	@Column(name = "`kodeicd`") public String icd10;
	@Column(name = "`namaicd`") public String keterangan;
	@Column(name = "`status`") public String status;
	@Column(name = "`deleted`") public String deleted;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="idOlahMr", referencedColumnName="id",insertable = false, updatable = false)
	
	
	public Integer getIdOlahMr() {
		return idOlahMr;
	}
	public Integer getIdDiagnosaOlahMr() {
		return idDiagnosaOlahMr;
	}
	public void setIdDiagnosaOlahMr(Integer idDiagnosaOlahMr) {
		this.idDiagnosaOlahMr = idDiagnosaOlahMr;
	}
	public void setIdOlahMr(Integer idOlahMr) {
		this.idOlahMr = idOlahMr;
	}
	
	public String getIcd10() {
		return icd10;
	}
	public void setIcd10(String icd10) {
		this.icd10 = icd10;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	

}






