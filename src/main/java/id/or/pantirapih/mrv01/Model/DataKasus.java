package id.or.pantirapih.mrv01.Model;

public class DataKasus {
	private Integer kasusBaru;
	private Integer kasusLama;
	private Integer jumlahKasus;
	private String tanggal;
	
	public Integer getKasusBaru() {
		return kasusBaru;
	}
	public void setKasusBaru(Integer kasusBaru) {
		this.kasusBaru = kasusBaru;
	}
	public Integer getKasusLama() {
		return kasusLama;
	}
	public void setKasusLama(Integer kasusLama) {
		this.kasusLama = kasusLama;
	}
	public Integer getJumlahKasus() {
		return jumlahKasus;
	}
	public void setJumlahKasus(Integer jumlahKasus) {
		this.jumlahKasus = jumlahKasus;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
}
