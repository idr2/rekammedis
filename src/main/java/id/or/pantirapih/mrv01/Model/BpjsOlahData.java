package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bpjsolahdata")
public class BpjsOlahData implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long idBpjsOlahData;
	@Column(name = "dokter_id")
	private Integer dokterId;
	@Column(name = "pasien_id")
	private Integer pasienId;
	@Column(name = "tanggalperiksa")
	private String tanggalPeriksa;
	@Column(name = "noregister")
	private String noRegister;
	@Column(name = "adddate")
	private String addDate;
	@Column(name = "updatedate")
	private String updateDate;
	@Column(name = "deleted")
	private Boolean deleted;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "bpjsolah_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Set<BpjsDiagnosa> bpjsDiagnosa;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "bpjsolah_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Set<BpjsTindakan> bpjsTindakan;
	
	public Long getIdBpjsOlahData() {
		return idBpjsOlahData;
	}
	public void setIdBpjsOlahData(Long idBpjsOlahData) {
		this.idBpjsOlahData = idBpjsOlahData;
	}
	public Integer getDokterId() {
		return dokterId;
	}
	public void setDokterId(Integer dokterId) {
		this.dokterId = dokterId;
	}
	public Integer getPasienId() {
		return pasienId;
	}
	public void setPasienId(Integer pasienId) {
		this.pasienId = pasienId;
	}
	public String getTanggalPeriksa() {
		return tanggalPeriksa;
	}
	public void setTanggalPeriksa(String tanggalPeriksa) {
		this.tanggalPeriksa = tanggalPeriksa;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getAddDate() {
		return addDate;
	}
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Set<BpjsDiagnosa> getBpjsDiagnosa() {
		return bpjsDiagnosa;
	}
	public void setBpjsDiagnosa(Set<BpjsDiagnosa> bpjsDiagnosa) {
		this.bpjsDiagnosa = bpjsDiagnosa;
	}
	public Set<BpjsTindakan> getBpjsTindakan() {
		return bpjsTindakan;
	}
	public void setBpjsTindakan(Set<BpjsTindakan> bpjsTindakan) {
		this.bpjsTindakan = bpjsTindakan;
	}

}
