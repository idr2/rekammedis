package id.or.pantirapih.mrv01.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bpjsdiagnosa")
public class BpjsDiagnosa implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long idBpjsDiagnosa;
	@Column(name = "bpjsolah_id")
	private Integer bpjsOlahId;
	@Column(name = "kodeicd")
	private String kodeIcd;
	@Column(name = "namaicd")
	private String namaIcd;
	@Column(name = "status")
	private String status;
	@Column(name = "adddate")
	private String addDate;
	@Column(name = "updatedate")
	private String updateDate;
	@Column(name = "deleted")
	private Boolean deleted;
	
	public Long getIdBpjsDiagnosa() {
		return idBpjsDiagnosa;
	}
	public void setIdBpjsDiagnosa(Long idBpjsDiagnosa) {
		this.idBpjsDiagnosa = idBpjsDiagnosa;
	}
	public Integer getBpjsOlahId() {
		return bpjsOlahId;
	}
	public void setBpjsOlahId(Integer bpjsOlahId) {
		this.bpjsOlahId = bpjsOlahId;
	}
	public String getKodeIcd() {
		return kodeIcd;
	}
	public void setKodeIcd(String kodeIcd) {
		this.kodeIcd = kodeIcd;
	}
	public String getNamaIcd() {
		return namaIcd;
	}
	public void setNamaIcd(String namaIcd) {
		this.namaIcd = namaIcd;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddDate() {
		return addDate;
	}
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
