package id.or.pantirapih.mrv01.Model;

public class DataPembayaran {
	
	private Integer Bpjs;
	private Integer Non;
	private Integer Bayar;
	private String tanggal;
	
	public Integer getBpjs() {
		return Bpjs;
	}
	public void setBpjs(Integer bpjs) {
		Bpjs = bpjs;
	}
	public Integer getNon() {
		return Non;
	}
	public void setNon(Integer non) {
		Non = non;
	}
	public Integer getBayar() {
		return Bayar;
	}
	public void setBayar(Integer bayar) {
		Bayar = bayar;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}

	
}
