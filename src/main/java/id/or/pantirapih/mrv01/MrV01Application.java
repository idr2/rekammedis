package id.or.pantirapih.mrv01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import id.or.pantirapih.mrv01.Controller.CORSFilter;


@SpringBootApplication
public class MrV01Application {
	
	public static void main(String[] args) {
		SpringApplication.run(MrV01Application.class, args);
	}

	@Bean
	FilterRegistrationBean corsFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(new CORSFilter());

		registration.setName("CORS Filter");
		registration.addUrlPatterns("/*");
		registration.setOrder(1);

		return registration;
	}
}

